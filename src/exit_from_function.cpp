#include "exit_from_function.h"
#include <QtWidgets>

exitFromFunction::exitFromFunction()
{
    m_type = type_exitFunction;
    m_color = QColor(0,200,255);
    addLinksPoints(QPoint(0,-20));
    addPresenceLink(false);
    setMaxInLink(1);
    setMaxOutLink(0);
}

exitFromFunction::~exitFromFunction()
{

}

void exitFromFunction::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{

    QColor fillColor = (option->state & QStyle::State_Selected) ? m_color.darker(150) : m_color;
    if (option->state & QStyle::State_MouseOver)
        fillColor = fillColor.lighter(125);

    painter->setRenderHint(QPainter::Antialiasing,true);
    painter->setPen(QPen(Qt::black,1.5));
    painter->setBrush(fillColor);
   // painter->drawRect(-30,-30,60,60);
    painter->drawEllipse(-20,-20,40,40);
    painter->setBrush(Qt::black);
    painter->drawEllipse(-10,-10,20,20);
    Q_UNUSED(option);
    Q_UNUSED(widget);
}

QRectF exitFromFunction::boundingRect() const
{
    return QRectF(-20,-20,40,40);
}

void exitFromFunction::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{

    QGraphicsObject::mouseMoveEvent(event);
    update();
    updateArrows();
}

void exitFromFunction::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mousePressEvent(event);
    this->setCursor(QCursor(Qt::ClosedHandCursor));
    Q_UNUSED(event);
}

void exitFromFunction::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{

    this->setCursor(QCursor(Qt::ArrowCursor));
    QGraphicsItem::mouseReleaseEvent(event);
    update();
}

void exitFromFunction::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    scene()->clearSelection();
    setSelected(true);
    getContextMenu()->exec(event->screenPos());
}

QPixmap exitFromFunction::image()
{
    QPixmap pixmap(100,100);
    pixmap.fill(Qt::transparent);
    QPainter painter(&pixmap);
    painter.setRenderHint(QPainter::Antialiasing,true);
    painter.setPen(QPen(Qt::black,4));
    painter.setBrush(QColor(0,200,200));
    painter.translate(60, 60);
    painter.drawEllipse(-30,-30,40,40);
    painter.setBrush(Qt::black);
    painter.drawEllipse(-20,-20,20,20);
    return pixmap;
}

void exitFromFunction::setTypeOperator(int type)
{
    baseOperator::setTypeOperator(type);
}

int exitFromFunction::getTypeOperator()
{
    return baseOperator::getTypeOperator();
}

void exitFromFunction::saveToXML(QDomElement &cn)
{
    baseOperator::saveToXML(cn);
}

void exitFromFunction::loadFromXML(QDomElement &cn)
{
    baseOperator::loadFromXML(cn);
}

