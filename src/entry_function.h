//#ifndef ENTRY_FUNCTION_H
//#define ENTRY_FUNCTION_H

#pragma once

#include "base_operator.h"

class entryFunction : public baseOperator
{
public:
    entryFunction();
    ~entryFunction();
protected:

    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);

public:
    virtual QPixmap image();
    virtual void setTypeOperator(int type);
    virtual int getTypeOperator();
    void saveToXML(QDomElement &cn);
    void loadFromXML(QDomElement &cn);
};

//#endif // ENTRY_FUNCTION_H
