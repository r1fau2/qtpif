#include "spec_symbol.h"
#include "container.h"
#include <QInputDialog>
#include <QRegExpValidator>
#include <QLineEdit>
#include <QLabel>
#include <QPushButton>
#include <QGridLayout>
#include <QtWidgets>


specSymbol::specSymbol()
{
    m_type = type_specSymbol;
    m_color = QColor(225,52,28);
    addLinksPoints(QPoint(-30,0));
    addLinksPoints(QPoint(0,-30));
    addLinksPoints(QPoint(30,0));
    addLinksPoints(QPoint(0,30));
    addPresenceLink(false);
    addPresenceLink(false);
    addPresenceLink(false);
    addPresenceLink(false);
    setMaxInLink(1);
    setMaxOutLink(1);
}

specSymbol::~specSymbol()
{

}

void specSymbol::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QColor fillColor = (option->state & QStyle::State_Selected) ? m_color.darker(150) : m_color;
    if (option->state & QStyle::State_MouseOver)
        fillColor = fillColor.lighter(125);

    painter->setRenderHint(QPainter::Antialiasing,true);
    painter->setPen(QPen(Qt::black,1.5));
    painter->setBrush(fillColor);
    painter->drawEllipse(-30,-30,60,60);
    QFont font = painter->font();
    font.setPixelSize(20);
    painter->setFont(font);
    painter->drawText(this->boundingRect(),Qt::AlignCenter,m_smb);
    Q_UNUSED(option);
    Q_UNUSED(widget);
}

QRectF specSymbol::boundingRect() const
{
    return QRectF (-30,-30,60,60);
}

void specSymbol::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsObject::mouseMoveEvent(event);
    update();
    updateArrows();
}

void specSymbol::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mousePressEvent(event);
    this->setCursor(QCursor(Qt::ClosedHandCursor));
    if (event->buttons() != Qt::LeftButton)
    {

        if(event->buttons() == Qt::RightButton)
        {
            container *c = container::instance();
            c->listItem.removeAll(this);
            this->~specSymbol();
            return;
        }
    }
    else return;

    Q_UNUSED(event);
}

void specSymbol::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    this->setCursor(QCursor(Qt::ArrowCursor));
    QGraphicsItem::mouseReleaseEvent(event);

    update();
}

void specSymbol::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    if (event->buttons() != Qt::LeftButton)
        return;

    QString str[] = {".", "+", "-", "*", "/", "|", "%", "..", "dup", "<", "<=", "=", "!=", ">=", ">", "?", "#", "true", "false", "type", "value", "bool", "char", "int", "float", "string", "()", "[]", "{}", "<()", "error", "in", "-1"};
    QStringList items;

    for(int i=0; str[i]!="-1"; i++)
          items << str[i];

          bool ok;
          QString buf = QInputDialog::getItem(NULL, tr("Специальный символ"),
                                               tr("Выберите:"), items, 0, false, &ok);
          if (ok && !buf.isEmpty())
              m_smb = buf;
}

void specSymbol::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    scene()->clearSelection();
    setSelected(true);
    getContextMenu()->exec(event->screenPos());
}

QPixmap specSymbol::image()
{
    QPixmap pixmap(100,100);
    pixmap.fill(Qt::transparent);
    QPainter painter(&pixmap);
    painter.setRenderHint(QPainter::Antialiasing,true);
    painter.setPen(QPen(Qt::black,4));
    painter.setBrush(QColor(225,52,28));
    painter.translate(50, 50);
    painter.drawEllipse(-30,-30,60,60);

    return pixmap;
}

void specSymbol::setTypeOperator(int type)
{
    baseOperator::setTypeOperator(type);
}

int specSymbol::getTypeOperator()
{
    return baseOperator::getTypeOperator();
}

void specSymbol::saveToXML(QDomElement &cn)
{
    baseOperator::saveToXML(cn);
    cn.setAttribute("data", m_smb);
}

void specSymbol::loadFromXML(QDomElement &cn)
{
    baseOperator::loadFromXML(cn);
    m_smb = cn.attribute("data");
}

QString specSymbol::getValue()
{
    return m_smb;
}

