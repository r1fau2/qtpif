#include "mainwindow.h"
#include <QApplication>
#include "container.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    container::instance(); //инициализация контейнера
    MainWindow MainWin;
    MainWin.setGeometry(100,100,600,500);
    MainWin.show();

    return a.exec();
}
