#include "arrow_item.h"
#include <math.h>
#include <QPen>
#include <QPainter>
#include "container.h"
#include "copy_operator.h"

const qreal Pi = 3.14;

arrowItem::arrowItem()
{
    m_startItem = NULL;
    m_endItem = NULL;
    setFlag(QGraphicsItem::ItemIsSelectable, true);
    setPen(QPen(m_color, 2, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
    m_type = TypeOperator::type_arrow;
    setZValue(5000.0);
    m_saved = false;
    m_startPoint = QPoint(0,0);
    m_endPoint = QPoint(0,0);
    m_arrowType = arrow;
}

arrowItem::arrowItem(baseOperator *startItem, baseOperator *endItem, QGraphicsItem *parent)
{
    m_startItem = startItem;
    m_endItem = endItem;
    setFlag(QGraphicsItem::ItemIsSelectable, true);
    setPen(QPen(m_color, 2, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
    m_type = TypeOperator::type_arrow;
    setZValue(5000.0);
    m_saved = false;
    m_startPoint = QPoint(0,0);
    m_endPoint = QPoint(0,0);
    m_arrowType = arrow;
}

arrowItem::~arrowItem()
{
    m_startItem->removeArrow(this);
    m_endItem->removeArrow(this);
}

QRectF arrowItem::boundingRect() const
{
    qreal extra = (pen().width() + 20) / 2.0;

    return QRectF(line().p1(), QSizeF(line().p2().x() - line().p1().x(),
                                      line().p2().y() - line().p1().y()))
            .normalized()
            .adjusted(-extra, -extra, extra, extra);
}

QPainterPath arrowItem::shape() const
{
    QPainterPath path = QGraphicsLineItem::shape();
    path.addPolygon(m_arrowHead);
    return path;
}

baseOperator *arrowItem::getStartItem() const
{
    return m_startItem;
}

baseOperator *arrowItem::getEndItem() const
{
    return m_endItem;
}

arrowItem::ArrowType arrowItem::getArrowType() const
{
    return m_arrowType;
}

void arrowItem::setArrowHeadFunc(const bool funcHead)
{
    if(funcHead == true)
        m_arrowType = circle;
    else
        m_arrowType = arrow;
}


TypeOperator arrowItem::getOperatorType() const
{
    return m_type;
}

void arrowItem::updatePosition()
{
    QLineF line(mapFromItem(m_startItem, m_startPoint), mapFromItem(m_endItem, m_endPoint));
    setLine(line);
}

void arrowItem::removeArrow(arrowItem *arrow)
{
    container *cont = container::instance();
    int index = cont->listArrows.indexOf(arrow);
    if (index != -1)
        cont->listArrows.removeAt(index);

    if(m_startItem->getTypeOperator() != type_copy_operator || m_startItem->getTypeOperator() != type_entryFunction)
         m_startItem->setPresenceLink(false, arrow->getStartPointNum());

    m_startItem->removeArrowOut(arrow);

    if(m_endItem->getTypeOperator() != type_groupList || m_endItem->getTypeOperator() != type_groupParList)
        m_endItem->setPresenceLink(false, arrow->getEndPointNum());

    m_endItem->removeArrowIn(arrow);

    if(m_endItem->getTypeOperator() == type_copy_operator)
    {
        copyOperator *c = qgraphicsitem_cast<copyOperator*>(m_endItem);
        c->setOperator(NULL);
        c->removeArrows();
    }
}

void arrowItem::saveToXML(QDomElement &cn)
{
    cn.setAttribute("idStartItem",QString::number(m_startItem->getId()));
    cn.setAttribute("idEndItem",QString::number(m_endItem->getId()));

    cn.setAttribute("startPoint_x",QString::number(m_startPoint.x()));
    cn.setAttribute("startPoint_y",QString::number(m_startPoint.y()));
    cn.setAttribute("endPoint_x",QString::number(m_endPoint.x()));
    cn.setAttribute("endPoint_y",QString::number(m_endPoint.y()));

    cn.setAttribute("startPointNum",QString::number(m_startPointNum));
    cn.setAttribute("endPointNum",QString::number(m_endPointNum));
    cn.setAttribute("arrowType", QString::number(m_arrowType));
}

void arrowItem::loadFromXML(QDomElement &cn)
{
    container *cont = container::instance();
    int idStartItem = cn.attribute("idStartItem").toInt();
    int idEndItem = cn.attribute("idEndItem").toInt();
    m_startPoint.setX(cn.attribute("startPoint_x").toInt());
    m_startPoint.setY(cn.attribute("startPoint_y").toInt());
    m_endPoint.setX(cn.attribute("endPoint_x").toInt());
    m_endPoint.setY(cn.attribute("endPoint_y").toInt());
    m_startPointNum = cn.attribute("startPointNum").toInt();
    m_endPointNum = cn.attribute("endPointNum").toInt();
    m_arrowType = (ArrowType)cn.attribute("arrowType").toInt();
    baseOperator *startItem = NULL;
    baseOperator *endItem = NULL;
    foreach (baseOperator *op, cont->listItem)
    {
        if(op->getId() == idStartItem)
            startItem = op;

        if(op->getId() == idEndItem)
            endItem = op;

        if(startItem != NULL && endItem != NULL)
        {
            m_startItem = startItem;
            m_endItem = endItem;
            startItem->addArrowOut(this);
            startItem->setPresenceLink(true,m_startPointNum);
            endItem->addArrowIn(this);
            endItem->setPresenceLink(true, m_endPointNum);
            this->updatePosition();
            break;
        }
    }
}

void arrowItem::setStartPoint(const QPointF point)
{
    m_startPoint = point;
    updatePosition();
}

void arrowItem::setEndPoint(const QPointF point)
{
    m_endPoint = point;
    updatePosition();
}

QPointF arrowItem::getStartPoint()
{
    return m_startPoint;
}

QPointF arrowItem::getEndPoint()
{
    return m_endPoint;
}

void arrowItem::setStartPointNum(const int num)
{
    m_startPointNum = num;
}

void arrowItem::setEndPointNum(const int num)
{
    m_endPointNum = num;
}

int arrowItem::getStartPointNum()
{
    return m_startPointNum;
}

int arrowItem::getEndPointNum()
{
    return m_endPointNum;
}

void arrowItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    if (m_startItem->collidesWithItem(m_endItem))
            return;

    Q_UNUSED(option);
    Q_UNUSED(widget);
    QPen myPen = pen();
    myPen.setColor(m_color);
    qreal arrowSize = 10;
    painter->setPen(myPen);
    painter->setBrush(m_color);
    painter->setRenderHint(QPainter::Antialiasing,true);
    painter->drawLine(line());

    if(m_arrowType == arrow) //проверку на то, какой конец линии отрисовывать
    {
        double angle = ::acos(line().dx() / line().length());
        if (line().dy() >= 0)
            angle = (Pi * 2) - angle;

        QPointF arrowP1 = line().p2() - QPointF(sin(angle + Pi / 3) * arrowSize,
                                                cos(angle + Pi / 3) * arrowSize);
        QPointF arrowP2 = line().p2() - QPointF(sin(angle + Pi - Pi / 3) * arrowSize,
                                                cos(angle + Pi - Pi / 3) * arrowSize);

        m_arrowHead.clear();
        m_arrowHead << line().p2() << arrowP1 << arrowP2;
        painter->drawPolygon(m_arrowHead);
    }
    else
    {
        painter->setBrush(Qt::white);
        painter->drawEllipse(line().p2(), 7, 7);
    }

/*
    // отрисовка изогнутой линии
    // remove to rem.txt 
*/
    if (isSelected()) {
        painter->setPen(QPen(Qt::black, 1, Qt::DashLine));
        QLineF myLine = line();
        qDebug()<<"Angle "<<line().angle();
        qDebug()<<"Line dx()/length: "<<abs(line().dx()/line().length());
        if( (220 > line().angle() && line().angle() > 140) || ( 40 > line().angle() && line().angle() >= 0) || ( line().angle() > 315 && line().angle() <= 359.9 ) )
        {
            myLine.translate(0, 5.0);
            painter->drawLine(myLine);
            myLine.translate(0,-10.0);
            painter->drawLine(myLine);
            qDebug()<<"Y block";

        }
        else
        {
            myLine.translate(5.0, 0);
            painter->drawLine(myLine);
            myLine.translate(-10.0, 0);
            painter->drawLine(myLine);
            qDebug()<<"X block";
        }
    }
}
