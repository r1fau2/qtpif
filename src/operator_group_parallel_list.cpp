#include "operator_group_parallel_list.h"
#include <QtWidgets>



operatorGroupParallelList::operatorGroupParallelList()
{
    m_type = type_groupParList;
    m_color = QColor(60,60,70);
    int step = 16;
    for(int i=0; i<10;i++)
    {
        addLinksPoints(QPoint(-80+step*i,0));
        addPresenceLink(false);
    }
    addLinksPoints(QPoint(0,0));
    addPresenceLink(false);
    setMaxInLink(-1);
    setMaxOutLink(1);
}

operatorGroupParallelList::~operatorGroupParallelList()
{

}

void operatorGroupParallelList::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(widget);


    QColor fillColor = (option->state & QStyle::State_Selected) ? QColor(Qt::black) : m_color;
    if (option->state & QStyle::State_MouseOver)
        fillColor = fillColor.lighter(175);

    QColor cl(QColor(60,60,70));

    painter->setRenderHint(QPainter::Antialiasing,true);
    painter->setPen(QPen(fillColor,3));
    //левая скобка
    const QPointF leftPoints[6] = {
                    QPointF(-80, -30),
                    QPointF(-90, -30),
                    QPointF(-90, -30),
                    QPointF(-90, 30),
                    QPointF(-90, 30),
                    QPointF(-80, 30)
    };

    //правая скобка
    const QPointF rightPoints[6] = {
                    QPointF(80, -30),
                    QPointF(90, -30),
                    QPointF(90, -30),
                    QPointF(90, 30),
                    QPointF(90, 30),
                    QPointF(80, 30)
    };

    painter->drawPolyline(leftPoints,6);
    painter->drawPolyline(rightPoints,6);
    painter->drawLine(QPointF(-90,0), QPointF(90,0));
}

QRectF operatorGroupParallelList::boundingRect() const
{
    return QRectF(-90,-30,183,63);
}

void operatorGroupParallelList::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{

    QGraphicsObject::mouseMoveEvent(event);
    update();
    updateArrows();
}

void operatorGroupParallelList::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mousePressEvent(event);
    this->setCursor(QCursor(Qt::ClosedHandCursor));
    Q_UNUSED(event);
}

void operatorGroupParallelList::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{

    this->setCursor(QCursor(Qt::ArrowCursor));
    QGraphicsItem::mouseReleaseEvent(event);
    update();
}

void operatorGroupParallelList::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    scene()->clearSelection();
    setSelected(true);
    getContextMenu()->exec(event->screenPos());
}

QPixmap operatorGroupParallelList::image()
{
    QPixmap pixmap(150,150);
    pixmap.fill(Qt::transparent);
    QPainter painter(&pixmap);
    painter.setRenderHint(QPainter::Antialiasing,true);
    painter.setPen(QPen(Qt::black,8));

    const QPointF leftPoints[6] = {
                    QPointF(-80, -30),
                    QPointF(-90, -30),
                    QPointF(-90, -30),
                    QPointF(-90, 30),
                    QPointF(-90, 30),
                    QPointF(-80, 30)
    };

    //правая скобка
    const QPointF rightPoints[6] = {
                    QPointF(35, -30),
                    QPointF(45, -30),
                    QPointF(45, -30),
                    QPointF(45, 30),
                    QPointF(45, 30),
                    QPointF(35, 30)
    };
    painter.translate(100, 75);
    painter.drawPolyline(leftPoints,6);
    painter.drawPolyline(rightPoints,6);
    painter.drawLine(QPointF(-90,0), QPointF(45,0));

    return pixmap;
}

void operatorGroupParallelList::setTypeOperator(int type)
{
    baseOperator::setTypeOperator(type);
}

int operatorGroupParallelList::getTypeOperator()
{
    return baseOperator::getTypeOperator();
}

void operatorGroupParallelList::saveToXML(QDomElement &cn)
{
    baseOperator::saveToXML(cn);
}

void operatorGroupParallelList::loadFromXML(QDomElement &cn)
{
    baseOperator::loadFromXML(cn);
}


