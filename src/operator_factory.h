//#ifndef OPERATOR_FACTORY_H
//#define OPERATOR_FACTORY_H

#pragma once

class GraphScene;
class baseOperator;
class QMenu;
namespace operator_factory {
    baseOperator *createItem(GraphScene *scene, int type);
    bool createFromXML(const QString &fileName, GraphScene *scene, QMenu *contextMenu);
    void saveToXML(const QString &fileName);
}

//#endif // OPERATOR_FACTORY_H
