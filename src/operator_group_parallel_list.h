//#ifndef OPERATOR_GROUP_PARALLEL_LIST_H
//#define OPERATOR_GROUP_PARALLEL_LIST_H

#pragma once

//#include "headers.h"
#include "base_operator.h"

class operatorGroupParallelList : public baseOperator
{
public:
    operatorGroupParallelList();
    ~operatorGroupParallelList();
private:

    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);
public:
    virtual QPixmap image();
    virtual void setTypeOperator(int type);
    virtual int getTypeOperator();
    void saveToXML(QDomElement &cn);
    void loadFromXML(QDomElement &cn);
};

//#endif // OPERATOR_GROUP_PARALLEL_LIST_H
