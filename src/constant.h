//#ifndef CONSTANT_H
//#define CONSTANT_H

#pragma once

//#include "headers.h"
#include "base_operator.h"

class Constant : public baseOperator
{
public:
    Constant();
    Constant(QGraphicsScene *scene);
    ~Constant();
protected:
    QGraphicsScene *m_scene;
    int m_value;

    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);
    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);

public:
    virtual QPixmap image();
    virtual void setTypeOperator(int type);
    virtual int getTypeOperator();
    void saveToXML(QDomElement &cn);
    void loadFromXML(QDomElement &cn);
    int getValue();
};

//#endif // CONSTANT_H
