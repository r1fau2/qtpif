#include "identifier.h"
#include <QInputDialog>
#include <QtWidgets>


identifier::identifier()
{
    m_type = type_identifier;
    m_color = QColor(0,200,200);
    m_name = "";
    addLinksPoints(QPoint(0,-20));
    addLinksPoints(QPoint(0,20));
    addPresenceLink(false);
    addPresenceLink(false);
    setMaxInLink(1);
    setMaxOutLink(1);
}

identifier::~identifier()
{

}

void identifier::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QColor color(QColor(0,200,200));
    QColor fillColor = (option->state & QStyle::State_Selected) ? color.darker(150) : color;
    if (option->state & QStyle::State_MouseOver)
        fillColor = fillColor.lighter(125);

    painter->setRenderHint(QPainter::Antialiasing,true);
    painter->setPen(QPen(Qt::black,2));
    painter->setBrush(fillColor);

    const QPointF points[5] = {
                    QPointF(-30, -20),
                    QPointF(-30, 5),
                    QPointF(0, 20),
                    QPointF(30, 5),
                    QPointF(30, -20)
    };

    painter->drawConvexPolygon(points, 5);
    painter->drawText(boundingRect().x(), boundingRect().y(), boundingRect().width(), boundingRect().height()-10,Qt::AlignCenter, m_name);
    Q_UNUSED(option);
    Q_UNUSED(widget);
}

QRectF identifier::boundingRect() const
{
    return QRectF(-30,-20,60+2,40+2);
    //2 - толщина пера
}

void identifier::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{

    QGraphicsObject::mouseMoveEvent(event);
    update();
    updateArrows();
}

void identifier::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mousePressEvent(event);
    this->setCursor(QCursor(Qt::ClosedHandCursor));
    Q_UNUSED(event);
}

void identifier::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{

    this->setCursor(QCursor(Qt::ArrowCursor));
    QGraphicsItem::mouseReleaseEvent(event);
    update();
}

void identifier::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    if (event->buttons() != Qt::LeftButton)
        return;
    bool ok;

    QString str = QInputDialog::getText(NULL, tr("Идентифиактор:"),"Название:", QLineEdit::Normal, m_name, &ok, Qt::Sheet);

    if(ok && !str.isEmpty())
        m_name = str;
}

void identifier::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    scene()->clearSelection();
    setSelected(true);
    getContextMenu()->exec(event->screenPos());
}


QPixmap identifier::image()
{
    QPixmap pixmap(100,100);
    pixmap.fill(Qt::transparent);
    QPainter painter(&pixmap);
    painter.setRenderHint(QPainter::Antialiasing,true);
    painter.setPen(QPen(Qt::black,4));
    painter.setBrush(QColor(0,200,200));
    painter.translate(50, 50);
    const QPointF points[5] = {
                    QPointF(-30, -20),
                    QPointF(-30, 5),
                    QPointF(0, 20),
                    QPointF(30, 5),
                    QPointF(30, -20)
    };

    painter.drawConvexPolygon(points, 5);
    return pixmap;
}

void identifier::setTypeOperator(int type)
{
    baseOperator::setTypeOperator(type);
}

int identifier::getTypeOperator()
{
    return baseOperator::getTypeOperator();
}

void identifier::saveToXML(QDomElement &cn)
{
    baseOperator::saveToXML(cn);
    cn.setAttribute("name", m_name);
}

void identifier::loadFromXML(QDomElement &cn)
{
    baseOperator::loadFromXML(cn);
    m_name = cn.attribute("name");
}

void identifier::setOperator(baseOperator *obj)
{
    m_operator = obj;
}

baseOperator *identifier::getOperator()
{
    if(m_operator == 0)
        return NULL;
    return m_operator;
}

void identifier::setName(const QString name)
{
    m_name = name;
}

QString identifier::getName()
{
    return m_name;
}

