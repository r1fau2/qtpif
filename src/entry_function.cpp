#include "entry_function.h"
#include "container.h"
#include <QtWidgets>


entryFunction::entryFunction()
{
    m_type = type_entryFunction;
    m_color = QColor(0,200,200);
    addLinksPoints(QPoint(0,20));
    addPresenceLink(false);
    setMaxInLink(0);
    setMaxOutLink(-1);
}


entryFunction::~entryFunction()
{

}

void entryFunction::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{

    QColor fillColor = (option->state & QStyle::State_Selected) ? m_color.darker(200) : m_color;
    if (option->state & QStyle::State_MouseOver)
        fillColor = fillColor.lighter(205);


    painter->setRenderHint(QPainter::Antialiasing,true);
    painter->setPen(QPen(Qt::black,1.5));
    painter->setBrush(fillColor);
    painter->drawEllipse(-20,-20,40,40);
    Q_UNUSED(option);
    Q_UNUSED(widget);
}

QRectF entryFunction::boundingRect() const
{
    return QRectF(-20,-20,40,40);
}

void entryFunction::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{

    QGraphicsObject::mouseMoveEvent(event);
    update();
    updateArrows();
}

void entryFunction::mousePressEvent(QGraphicsSceneMouseEvent *event)
{

    this->setCursor(QCursor(Qt::ClosedHandCursor));

        if(event->buttons() == Qt::RightButton)
        {
            container *c = container::instance();
            c->listItem.removeAll(this);
            this->~entryFunction();
            return;
        }

    QGraphicsItem::mousePressEvent(event);
}

void entryFunction::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{

    this->setCursor(QCursor(Qt::ArrowCursor));
    QGraphicsItem::mouseReleaseEvent(event);
    update();
}

void entryFunction::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    scene()->clearSelection();
    setSelected(true);
    getContextMenu()->exec(event->screenPos());
}

QPixmap entryFunction::image()
{
    QPixmap pixmap(100,100);
    pixmap.fill(Qt::transparent);
    QPainter painter(&pixmap);
    painter.setRenderHint(QPainter::Antialiasing,true);
    painter.setPen(QPen(Qt::black,4));
    painter.setBrush(QColor(0,200,200));
    painter.translate(60, 60);
    painter.drawEllipse(-30,-30,40,40);

    return pixmap;
}

void entryFunction::setTypeOperator(int type)
{
    baseOperator::setTypeOperator(type);
}

int entryFunction::getTypeOperator()
{
    return baseOperator::getTypeOperator();
}

void entryFunction::saveToXML(QDomElement &cn)
{
    baseOperator::saveToXML(cn);
    qDebug()<<"Вход в функцию записал xml";
}

void entryFunction::loadFromXML(QDomElement &cn)
{
    baseOperator::loadFromXML(cn);
}


