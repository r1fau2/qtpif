//#ifndef GRAPHSCENE_H
//#define GRAPHSCENE_H

#pragma once

#include <QGraphicsScene>

#include <QGraphicsSceneMouseEvent>
#include "operator_factory.h"
#include "list_area.h"
#include "mainwindow.h"

#include <QGraphicsGridLayout>
#include <QGraphicsWidget>

QT_BEGIN_NAMESPACE
class QGraphicsSceneMouseEvent;
class QMenu;
class QPointF;
class QGraphicsLineItem;
class QFont;
class QGraphicsTextItem;
class QColor;
class MainWindow;
class listArea;
QT_END_NAMESPACE


class GraphScene : public QGraphicsScene
{
    Q_OBJECT
public:
    explicit GraphScene(QMenu *itemMenu, MainWindow *parent);
    enum Mode { None, InsertItem, InsertLine , InsertArea, MoveItem };
    void setIdItemCreate(int id);
    int getIdItem();
    void setMyMode(Mode m);
    Mode getMode() {return myMode; }
//    void updateAreas(listArea *arlist);
    void updateScene();
    QMenu *getItemContextMenu();
    //void writeContentToPngFile(const QString &fileName);
    void resetScene();
    bool setArrowHead(arrowItem *arrow, baseOperator *startItem, baseOperator *endItem);
    bool checkLink(baseOperator *start, baseOperator *end, arrowItem *arrow);
    bool setLinePointAtItem(baseOperator *start, baseOperator *end, QPointF const startPoint, QPointF const endPoint, arrowItem *arrow);
    float distToPoint(QPointF const start, QPointF const end);
    void writeLink(QTextStream &stream, QList<baseOperator*> *listConst, QList<baseOperator*> *listFunc, baseOperator *op);
    void writeLink_copyOp(QTextStream &stream, QList<baseOperator*> *listConst, QList<baseOperator*> *listFunc, baseOperator *obj);
   //
public slots:
    void areasUpdate();
    void updateDelayItem();
    void generateRIG();
signals:
    void releaseButtOperator(int);
    void checkChanged();

protected:
    virtual void keyPressEvent(QKeyEvent *event);
    virtual void mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent);
    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent);
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent);
   //    virtual void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *mouseEvent);
    MainWindow          *m_parent;
    baseOperator        *m_selected;
    QMenu               *m_ItemMenu;
    Mode                myMode;
    int                 id_createItem;
    //QPointF             startPoint;
    QGraphicsLineItem   *line;
    QPainterPath        m_selectedArea;
    QPointF             m_startLinePoint;
    QPointF             m_endLinePoint;
//
private:
    QGraphicsGridLayout *layout;
    int i;
    int j;
//
};

//#endif // GRAPHSCENE_H
