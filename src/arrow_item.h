//#ifndef ARROW_ITEM_H
//#define ARROW_ITEM_H

#pragma once

//#include <QGraphicsLineItem>
#include "base_operator.h"

QT_BEGIN_NAMESPACE
class QGraphicsPolygonItem;
class QGraphicsLineItem;
class QGraphicsScene;
class QRectF;
class QGraphicsSceneMouseEvent;
class QPainterPath;
class QDomElement;
QT_END_NAMESPACE

class arrowItem : public QGraphicsLineItem
{
public:
    enum ArrowType { arrow, circle };
    enum { Type = UserType + 1 };
    arrowItem();
    arrowItem(baseOperator *startItem, baseOperator *endItem, QGraphicsItem *parent = 0);
    ~arrowItem();
    QRectF boundingRect() const override;
    QPainterPath shape() const override;
    baseOperator *getStartItem() const;
    baseOperator *getEndItem() const;
    ArrowType getArrowType() const;
    void setArrowHeadFunc(bool const funcHead);
    int type() const override { return Type; }
    TypeOperator getOperatorType() const;
    void updatePosition();
    void removeArrow(arrowItem *arrow);
    void saveToXML(QDomElement &cn);
    void loadFromXML(QDomElement &cn);
    void setStartPoint(QPointF const point);
    void setEndPoint(QPointF const point);
    QPointF getStartPoint();
    QPointF getEndPoint();
    void setStartPointNum(int const num);
    void setEndPointNum(int const num);
    int getStartPointNum();
    int getEndPointNum();
protected:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
    baseOperator *m_startItem;
    baseOperator *m_endItem;
    QPointF       m_startPoint;
    QPointF       m_endPoint;
    QPolygonF     m_arrowHead;
    ArrowType     m_arrowType;
    TypeOperator  m_type;
    QColor        m_color;
    bool          m_saved;
    int           m_startPointNum;
    int           m_endPointNum;
};

//#endif // ARROW_ITEM_H
