//#ifndef EXIT_FROM_FUNCTION_H
//#define EXIT_FROM_FUNCTION_H

#pragma once

//#include "headers.h"
#include "base_operator.h"

class exitFromFunction : public baseOperator
{
public:
    exitFromFunction();
    ~exitFromFunction();
protected:

    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);

public:
    virtual QPixmap image();
    virtual void setTypeOperator(int type);
    virtual int getTypeOperator();
    void saveToXML(QDomElement &cn);
    void loadFromXML(QDomElement &cn);

};

//#endif // EXIT_FROM_FUNCTION_H
