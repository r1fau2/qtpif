#include "interpret_with_func_introduced.h"
#include <QInputDialog>
#include <QtWidgets>
#include "arrow_item.h"

interpretWithFuncIntroduced::interpretWithFuncIntroduced()
{
    m_funcName = "";
    m_type = type_interpret_func;
    m_color = QColor(225,52,28);
    setWidht(100);
    setHeight(60);
    m_widhtAdd = 0;
    oldRightPoint = rightPoint = QPoint(50,0);
    oldLeftPoint = leftPoint = QPoint(-50,0);
    addLinksPoints(leftPoint);
    addLinksPoints(QPoint(0,-30));
    addLinksPoints(rightPoint);
    addLinksPoints(QPoint(0,30));
    addPresenceLink(false);
    addPresenceLink(false);
    addPresenceLink(false);
    addPresenceLink(false);
    setMaxInLink(1);
    setMaxOutLink(1);
}

interpretWithFuncIntroduced::~interpretWithFuncIntroduced()
{

}

void interpretWithFuncIntroduced::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{

    QColor fillColor = (option->state & QStyle::State_Selected) ? m_color.darker(150) : m_color;
    if (option->state & QStyle::State_MouseOver)
        fillColor = fillColor.lighter(125);


    painter->setRenderHint(QPainter::Antialiasing,true);
    painter->setPen(QPen(Qt::black,2));
    painter->setBrush(fillColor);

    painter->drawRoundedRect(-50-m_widhtAdd/2, -30, 100 +m_widhtAdd, 60, 30, 30);
    painter->drawText(this->boundingRect(),Qt::AlignCenter,m_funcName);
    Q_UNUSED(option);
    Q_UNUSED(widget);
}

QRectF interpretWithFuncIntroduced::boundingRect() const
{
    return QRectF(-50-m_widhtAdd/2, -30, 100+m_widhtAdd, 60);
}

void interpretWithFuncIntroduced::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{

    QGraphicsObject::mouseMoveEvent(event);
    update();
    updateArrows();
}

void interpretWithFuncIntroduced::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mousePressEvent(event);
    this->setCursor(QCursor(Qt::ClosedHandCursor));
    Q_UNUSED(event);
}

void interpretWithFuncIntroduced::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{

    this->setCursor(QCursor(Qt::ArrowCursor));
    QGraphicsItem::mouseReleaseEvent(event);
    update();
}

void interpretWithFuncIntroduced::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    if (event->buttons() != Qt::LeftButton)
        return;
    bool ok;

    QString str = QInputDialog::getText(NULL, tr("Внешняя функция:"),"Название:", QLineEdit::Normal, m_funcName, &ok, Qt::Sheet);

    if(ok && !str.isEmpty())
        m_funcName = str;
    int count = m_funcName.size() - 12;
    if(count > 0)
    {
        m_widhtAdd = count*7;
    }
    else m_widhtAdd = 0;

    oldLeftPoint = leftPoint;
    oldRightPoint = rightPoint;

    leftPoint.setX(-50 - m_widhtAdd/2);
    rightPoint.setX(50 + m_widhtAdd/2);
    setLinksPoints(leftPoint, 0);
    setLinksPoints(rightPoint, 2);

    if(getCountArrowIn() != 0)
    {

        arrowItem *arrowIn = getArrowIn(0);

        QPointF pointIn = arrowIn->getEndPoint();

        if(pointIn == oldLeftPoint)
            arrowIn->setEndPoint(leftPoint);

        if(pointIn == oldRightPoint)
            arrowIn->setEndPoint(rightPoint);
    }

    if(getCountArrowOut() != 0)
    {
        arrowItem *arrowOut = getArrowOut(0);
        QPointF pointOut = arrowOut->getStartPoint();

        if(pointOut == oldLeftPoint)
            arrowOut->setStartPoint(leftPoint);

        if(pointOut == oldRightPoint)
            arrowOut->setStartPoint(rightPoint);
    }

    qDebug()<< "Razmer funcName :"<<m_funcName.size();
}

void interpretWithFuncIntroduced::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    scene()->clearSelection();
    setSelected(true);
    getContextMenu()->exec(event->screenPos());
}

QPixmap interpretWithFuncIntroduced::image()
{
    QPixmap pixmap(140,140);
    pixmap.fill(Qt::transparent);
    QPainter painter(&pixmap);
    painter.setRenderHint(QPainter::Antialiasing,true);
    painter.setPen(QPen(Qt::black,4));
    painter.setBrush(QColor(225,52,28));
    painter.translate(50, 70);
    painter.drawRoundedRect(-30,-30,100,60,30,30);
    return pixmap;
}

void interpretWithFuncIntroduced::setTypeOperator(int type)
{
    baseOperator::setTypeOperator(type);
}

int interpretWithFuncIntroduced::getTypeOperator()
{
    return baseOperator::getTypeOperator();
}

void interpretWithFuncIntroduced::saveToXML(QDomElement &cn)
{
    baseOperator::saveToXML(cn);
    cn.setAttribute("funcName", m_funcName);
    cn.setAttribute("widhtAdd", m_widhtAdd);

}

void interpretWithFuncIntroduced::loadFromXML(QDomElement &cn)
{
    baseOperator::loadFromXML(cn);
    m_funcName = cn.attribute("funcName");
    m_widhtAdd = cn.attribute("widhtAdd").toInt();
}

QString interpretWithFuncIntroduced::getFuncName()
{
    return m_funcName;
}

