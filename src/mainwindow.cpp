#include "mainwindow.h"
#include <QtGui>
#include <QtWidgets>
#include <QStyle>
#include "arrow_item.h"
#include "container.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    menuBar = new QMenuBar();
    setMinimumSize(600,500);
    setWindowTitle("IDE");


    createMenu();
    createToolbars();

    m_scene = new GraphScene(itemMenu, this);
    m_scene->setSceneRect(QRectF(0,0,5000,5000));
    //связывание сигнала класса окна и сцены для отжатия клавиши
    connect(m_scene, SIGNAL(releaseButtOperator(int)), this, SLOT(releaseButtOperator(int)));

    connect(generate, SIGNAL(triggered()), m_scene, SLOT(generateRIG()));

    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(toolBox);
    view = new QGraphicsView(m_scene);
    layout->addWidget(view);
    view->setDragMode(QGraphicsView::RubberBandDrag);
    view->setInteractive(true);
    view->hide();
    toolBox->hide();
    QWidget *widget = new QWidget;
    widget->setLayout(layout);
    setCentralWidget(widget);
    setUnifiedTitleAndToolBarOnMac(true);
    m_contentChanged = false;
}

MainWindow::~MainWindow()
{

}

void MainWindow::contentChanged(const bool &changed)
{

    if (m_contentChanged == false && changed == true)
    {
        setWindowTitle(windowTitle().prepend("* "));
        m_contentChanged = true;

        QFileInfo fileInfo(m_fileName);
        if ((m_fileName != tr("untitled") || m_fileName != tr("* untitled")) && fileInfo.isWritable())
            saveFileAction->setEnabled(true);
    }
    else if (m_contentChanged == true && changed == false)
    {
        setWindowTitle(windowTitle().remove(0,2));
        m_contentChanged = false;
        saveFileAction->setEnabled(false);
    }
}

void MainWindow::createActions()
{

    newFile = new QAction(tr("&Создать новый проект"), this);
    newFile->setStatusTip(tr("Создать новый проект"));
    newFile->isVisible();
    connect(newFile, SIGNAL(triggered(bool)), this, SLOT(newProject()));

    openFileAction = new QAction(tr("&Открыть"), this);
    openFileAction->setShortcut(tr("Ctrl+O"));
    openFileAction->setStatusTip(tr("Открыть диаграмму"));
    openFileAction->isVisible();
    connect(openFileAction, SIGNAL(triggered()), this, SLOT(openProject()));

    saveFileAction = new QAction(tr("&Сохранить"), this);
    saveFileAction->setShortcut(tr("Ctrl+S"));
    saveFileAction->setStatusTip(tr("Сохранение текущей диаграммы"));
    saveFileAction->isVisible();
    saveFileAction->setEnabled(false);
    connect(saveFileAction, SIGNAL(triggered()), this, SLOT(saveProject()));

    saveFileToPathAction = new QAction(tr("&Сохранить по пути..."), this);
    saveFileToPathAction->setStatusTip(tr("Сохранение текущей диаграммы"));
    saveFileToPathAction->isVisible();
    saveFileToPathAction->setEnabled(false);
    connect(saveFileToPathAction, SIGNAL(triggered()), this, SLOT(SaveProjectAtPath()));


    closeFile = new QAction(tr("&Закрыть проект"), this);
    closeFile->setStatusTip(tr("Закрыть текущий проект"));
    closeFile->isVisible();
    closeFile->setEnabled(false);
    connect(closeFile, SIGNAL(triggered()), this, SLOT(closeProject()));



    exitAction = new QAction(tr("&Выход"), this);
    exitAction->setShortcut(tr("Ctrl+Q"));
    exitAction->setStatusTip(tr("Quit IDE"));
    connect(exitAction, SIGNAL(triggered()), this, SLOT(closeWindow()));


    clearAction = new QAction(tr("&Очистить рабочую область"), this);
    clearAction->setStatusTip(tr("Очистить рабочую область"));
    clearAction->isVisible();
    clearAction->setEnabled(false);
    connect(clearAction, SIGNAL(triggered()), this, SLOT(clearArea()));


    generate = new QAction(tr("&Сгенерировать РИГ"), this);
    generate->setShortcut(tr("Ctrl+L"));
    generate->isVisible();
    generate->setEnabled(false);




    aboutAction = new QAction(tr("&О программе"),this);
    aboutAction->setShortcut(tr("F1"));
    connect(aboutAction, SIGNAL(triggered()), this, SLOT(about()));

    deleteAction = new QAction(tr("&Удалить выбранное"),this);
    deleteAction->setStatusTip(tr("Удалить выбранныq объект"));
    deleteAction->setIcon(QIcon(":/images/delete.png"));
    connect(deleteAction, SIGNAL(triggered()), this, SLOT(deleteItems()));
}

void MainWindow::createMenu()
{
    createActions();
    createToolBox();

    //File
    fileMenu = new  QMenu("&Файл");

    fileMenu->addAction(newFile);
    fileMenu->addAction(openFileAction);
    fileMenu->addAction(saveFileAction);
    fileMenu->addAction(saveFileToPathAction);
    fileMenu->addAction(closeFile);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAction);
    menuBar->addMenu(fileMenu);


    actionMenu = new QMenu("&Действия");
    actionMenu->addAction(clearAction);
    actionMenu->addSeparator();
    actionMenu->addAction(generate);
    menuBar->addMenu(actionMenu);


    itemMenu = new QMenu();
    itemMenu->addAction(deleteAction);

    //About
    aboutMenu = new QMenu("&О программе");
    aboutMenu->addAction(aboutAction);
    menuBar->addMenu(aboutMenu);

    this->setMenuBar(menuBar);
    menuBar->show();


}

void MainWindow::createToolBox()
{
    buttonGroup = new QButtonGroup(this);
    buttonGroup->setExclusive(false);

    QGridLayout *layout = new QGridLayout;

    connect(buttonGroup, SIGNAL(buttonClicked(int)), this, SLOT(buttonGroupClicked(int)));

    layout->addWidget(addCellOperator(tr("Вход в функцию"),0),                       0, 0);
    layout->addWidget(addCellOperator(tr("Выход из функции"),1),                     0, 1);
    layout->addWidget(addCellOperator(tr("Константа"),2),                            1, 0);
    layout->addWidget(addCellOperator(tr("Оператор <br>копирования"),3),             1, 1);
    layout->addWidget(addCellOperator(tr("Внешняя функция"),4),                      2, 0);
    layout->addWidget(addCellOperator(tr("Идентификатор"),5),                        2, 1);
    layout->addWidget(addCellOperator(tr("Интерпретатор"),6),                        3, 0);
    layout->addWidget(addCellOperator(tr("Интерпретатор<br>с аргументом"),7),              3, 1);
    layout->addWidget(addCellOperator(tr("Интерпретатор<br> с арг. и функцией"),8),    4, 0);
    layout->addWidget(addCellOperator(tr("Интерпретатор <br>с функцией"),9),         4, 1);
    layout->addWidget(addCellOperator(tr("&nbsp;&nbsp;&nbsp;&nbsp;"
                                         "&nbsp;&nbsp;&nbsp;Оператор"
                                         " <br>группировки в лист"),10),             5, 0);
    layout->addWidget(addCellOperator(tr("Оператор группировки<br> в параллельный лист"),11),   5, 1);
    layout->addWidget(addCellOperator(tr("Специальный<br>символ"),12),               6, 0);
    layout->addWidget(addCellOperator(tr("Зона задержки"),13),                       6, 1);
    layout->addWidget(addCellOperator(tr("Функциональная<br>константа"),14),         7, 0);

    QWidget *wid = new QWidget;
    wid->setLayout(layout);


    toolBox = new QToolBox;
    toolBox->setSizePolicy(QSizePolicy(QSizePolicy::Maximum, QSizePolicy::Ignored));
    toolBox->setMinimumWidth(wid->sizeHint().width()+20);
    toolBox->addItem(wid, style()->standardIcon(QStyle::SP_FileDialogListView), tr("Операторы"));
}

void MainWindow::createToolbars()
{
    editToolBar = addToolBar(tr("delete"));

    editToolBar->addAction(deleteAction);

    QToolButton *pointerButton = new QToolButton;
    pointerButton->setCheckable(true);
    pointerButton->setChecked(true);//курсор
    pointerButton->setIcon(style()->standardIcon(QStyle::SP_MediaPlay));
    QToolButton *linePointerButton = new QToolButton;
    linePointerButton->setCheckable(true);//линия
    linePointerButton->setIcon(style()->standardIcon(QStyle::SP_LineEditClearButton));

    pointerTypeGroup = new QButtonGroup(this);
    pointerTypeGroup->addButton(pointerButton,GraphScene::Mode::MoveItem);
    pointerButton->setIcon(QIcon(":/images/pointer.png"));
    pointerTypeGroup->addButton(linePointerButton,GraphScene::Mode::InsertLine);
    linePointerButton->setIcon(QIcon(":/images/linepointer.png"));
    connect(pointerTypeGroup, SIGNAL(buttonClicked(int)), this, SLOT(pointerGroupClicked(int)));

    sceneScaleCombo = new QComboBox;
    QStringList scales;
    scales << tr("50%") << tr("75%") << tr("100%") << tr("125%") << tr("150%");
    sceneScaleCombo->addItems(scales);
    sceneScaleCombo->setCurrentIndex(2);

    connect(sceneScaleCombo, SIGNAL(currentIndexChanged(QString)), this, SLOT(sceneScaleChanged(QString)));

    pointerToolbar = addToolBar(tr("Mode"));
    pointerToolbar->addWidget(pointerButton);
    pointerToolbar->addWidget(linePointerButton);
    pointerToolbar->addWidget(sceneScaleCombo);
}

void MainWindow::releaseButtOperator(int id)
{
    QList <QAbstractButton *> buttons = buttonGroup->buttons();
    int i = 0;
    foreach (QAbstractButton *button, buttons) {
        if(i == id){
            button->setChecked(false);
            break;
        }
        i++;
    }
}

void MainWindow::newProject()
{
    if (!closeProject())
            return;

    container *cont = container::instance();
    cont->clearListItem();
    m_scene->resetScene();
    qDebug()<<"Видимы";
    saveFileAction->setEnabled(false);
    saveFileToPathAction->setEnabled(true);
    clearAction->setEnabled(true);
    generate->setEnabled(true);
    closeFile->setEnabled(true);
    contentChanged(false);
    view->setVisible(true);
    toolBox->setVisible(true);
    setWindowTitle("untitled");
}

void MainWindow::openProject(const QString &fileName)
{
    if (!closeProject())
            return;


        QString currFilename(m_fileName);
        if (fileName.isEmpty())
        {
            QFileDialog dialog(this,
                               tr("Открыть проект"),
                               QDir::homePath(),
                               QString("PifagorIDE (*.vpfg)"));
            dialog.setAcceptMode(QFileDialog::AcceptOpen);
            dialog.setDefaultSuffix("vpfg");

            if (!dialog.exec())
                return;

            m_fileName = dialog.selectedFiles().first();
        }
        else
        {
            m_fileName = fileName;
        }

        QFileInfo fileInfo(m_fileName);
        if (!fileInfo.isWritable())
            //statusBarMsg(tr("Read-only file!"));
            qDebug()<<"Только для чтения!";

        container *cont = container::instance();
        cont->clearListItem();
        m_scene->resetScene();
        if (!operator_factory::createFromXML(m_fileName, m_scene, itemMenu))
        {
            m_fileName = currFilename;
            return;
        }

        contentChanged(false);

        fileInfo.isWritable() ?
             setTitle(m_fileName) :
             setTitle(tr("readonly ").append(m_fileName));

       saveFileAction->setEnabled(false);
       saveFileToPathAction->setEnabled(true);
       clearAction->setEnabled(true);
       generate->setEnabled(true);
       closeFile->setEnabled(true);

       view->setVisible(true);
       toolBox->setVisible(true);

       m_scene->setMyMode(GraphScene::MoveItem);
       m_scene->update();
}

void MainWindow::saveProject(const bool &checkIfReadonly)
{
    QFileInfo fileInfo(m_fileName);
    if (checkIfReadonly && !fileInfo.isWritable())
    {
        qDebug()<<"сохранение не удалось";
        return;
    }

    operator_factory::saveToXML(m_fileName);
    contentChanged(false);
}

bool MainWindow::SaveProjectAtPath()
{
    QFileDialog dialog(this, tr("Сохранить проект"), QDir::homePath(), QString("PifagorIDE (*.vpfg)"));
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    dialog.setDefaultSuffix("vpfg");

    if (!dialog.exec())
        return false;

    m_fileName = dialog.selectedFiles().first();
    saveProject(false);
    setTitle(m_fileName);
    return true;
}

void MainWindow::clearArea()
{
    container *cont = container::instance();
    cont->clearListItem();
    m_scene->resetScene();
}

void MainWindow::generateRIG()
{
    m_scene->updateDelayItem();
}

void MainWindow::closeWindow()
{

}

bool MainWindow::closeProject()
{
    if (m_contentChanged)
    {
        QMessageBox msgBox(this);
        msgBox.setWindowTitle(tr("Закрытие проекта"));
        msgBox.setText(tr("Граф был изменен."));
        msgBox.setInformativeText(tr("Сохранить изменения?"));
        msgBox.setStandardButtons(QMessageBox::Save |
                                  QMessageBox::No |
                                  QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Save);
        int ret = msgBox.exec();

        switch (ret) {
        case QMessageBox::Save:
        {
            if (m_fileName == tr("untitled"))
            {
                if (!SaveProjectAtPath())
                        return false;
            }
            else
            {
                saveProject();
            }

            break;
        }
        case QMessageBox::No    :
            break;
        case QMessageBox::Cancel:
            return false;
        default:
            break;
        }
    }

    saveFileAction->setEnabled(false);
    saveFileToPathAction->setEnabled(false);
    clearAction->setEnabled(false);
    generate->setEnabled(false);
    closeFile->setEnabled(false);
    view->setVisible(false);
    toolBox->setVisible(false);
    qDebug()<<"Спряталось";
    m_contentChanged = false;
    setTitle("");
    container *cont = container::instance();
    cont->clearListItem();
    m_scene->resetScene();
    view->hide();
    toolBox->setVisible(false);
    return true;
}

void MainWindow::setTitle(const QString &title)
{
    title.isEmpty() ?
         setWindowTitle("PifagorIDE") :
         setWindowTitle(QString(title).append(" - PifagorIDE"));
}

QWidget *MainWindow::addCellOperator(const QString &text, int id)
{
        baseOperator *op = operator_factory::createItem(NULL,id);
        QIcon icon(op->image());
        QToolButton *button = new QToolButton;
        button->setIcon(icon);
        button->setIconSize(QSize(50, 50));
        button->setCheckable(true);
        buttonGroup->addButton(button,id /*int(type)*/);

        QGridLayout *layout = new QGridLayout;
        layout->addWidget(button, 0, 0, Qt::AlignHCenter);
        layout->addWidget(new QLabel(text), 1, 0, Qt::AlignCenter);

        QWidget *widget = new QWidget;
        widget->setLayout(layout);
        delete op;
        return widget;
}

void MainWindow::about()
{
    QMessageBox::about(this, tr("About program"),
                       tr("Графическая среда разработки функционально-потоковых"
                          " параллельных программ для ЯП <strong>'Пифагор'</strong>"));
}

void MainWindow::buttonGroupClicked(int id) //поиск нажатой кнопки
{
    if(m_scene->getMode() == GraphScene::InsertLine)
         pointerGroupClicked(GraphScene::MoveItem);
    QList <QAbstractButton *> buttons = buttonGroup->buttons();
    int i=0;
    //для проверки, были ли до этого нажата одна из кнопок, если getIdItem != -1,
    //то до текущего входа в функцию был выбран элемент, но не помещен на сцену(перевыбор)
    bool fl = false;
    if(m_scene->getIdItem() != -1)
        fl = true;

    foreach (QAbstractButton *button, buttons)
    {
        if(buttonGroup->button(id) != button)
        {
            button->setChecked(false);
            i++;
        }
        else
        {
            if(m_scene->getIdItem() == -1) //создать или перевыбрать
            {
                button->setChecked(true);
                m_scene->setIdItemCreate(i);
                if( i == 13 )
                    m_scene->setMyMode(GraphScene::InsertArea);
                else
                    m_scene->setMyMode(GraphScene::InsertItem);
                if(fl == false)
                    break;
            }
            if(m_scene->getIdItem() == i && fl == true)
            {
                m_scene->setIdItemCreate(-1);
                m_scene->setMyMode(GraphScene::MoveItem);
                button->setChecked(false);
                break;
            }

            if(m_scene->getIdItem() != i && fl == true)
            {
                button->setChecked(true);
                m_scene->setIdItemCreate(i);
                if( i == 13 ) //если id=13 - вставка выделения
                    m_scene->setMyMode(GraphScene::InsertArea);
                else
                    m_scene->setMyMode(GraphScene::InsertItem);
            }
        }
    }
}

void MainWindow::pointerGroupClicked(int selectMode)
{

    m_scene->setMyMode(GraphScene::Mode(selectMode));

    if(selectMode == GraphScene::InsertLine)
    {
        view->setDragMode(QGraphicsView::NoDrag);
        pointerTypeGroup->button(GraphScene::Mode::MoveItem)->setChecked(false);
        pointerTypeGroup->button(GraphScene::Mode::InsertLine)->setChecked(true);
    }
    else
    {
        view->setDragMode(QGraphicsView::RubberBandDrag);
        pointerTypeGroup->button(GraphScene::MoveItem)->setChecked(true);
        pointerTypeGroup->button(GraphScene::InsertLine)->setChecked(false);
    }
}

//удаление выбранных элементов со сцены
void MainWindow::deleteItems()
{
    if(m_scene->selectedItems().isEmpty())
    {
        QMessageBox::critical(this, tr("Удаление"),
                           tr("Не выделено ни одного элемента."));
        return;
    }

    foreach (QGraphicsItem *item, m_scene->selectedItems())
    {
        if (item->zValue() == 5000.0)
        {
            //scene->removeItem(item);
            arrowItem *arrow = qgraphicsitem_cast<arrowItem*>(item);
            arrow->removeArrow(arrow);
            delete item;
        }
    }

    //добавить
    foreach (QGraphicsItem *item, m_scene->selectedItems())
    {
        baseOperator *op = qgraphicsitem_cast<baseOperator*>(item);
        op->removeItem();
        //если выделенная область, удаляем из списка областей
        if(op->getTypeOperator() == TypeOperator::type_listArea)
        {
            container *cont = container::instance();
            listArea *arlist = qgraphicsitem_cast<listArea*>(op);
            int index =  cont->listAreas.indexOf(arlist);
            if(index != -1)
                cont->listAreas.removeAt(index);
        }
        delete item;
    }
}

void MainWindow::sceneScaleChanged(QString scale)
{
    double newScale = scale.left(scale.indexOf(tr("%"))).toDouble() / 100.0;
        QMatrix oldMatrix = view->matrix();
        view->resetMatrix();
        view->translate(oldMatrix.dx(), oldMatrix.dy());
        view->scale(newScale, newScale);
}
