//#ifndef EXTERNAL_FUNCTION_H
//#define EXTERNAL_FUNCTION_H

#pragma once

//#include "headers.h"
#include "base_operator.h"


class external_function : public baseOperator
{
public:
    external_function();
    ~external_function();
protected:
    int m_widhtAdd;
    QString m_funcName;
    QPointF leftPoint;
    QPointF rightPoint;
    QPointF oldLeftPoint;
    QPointF oldRightPoint;
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);
    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);

public:
    virtual QPixmap image();
    virtual void setTypeOperator(int type);
    virtual int getTypeOperator();
    void saveToXML(QDomElement &cn);
    void loadFromXML(QDomElement &cn);
    QString getFuncName();

};

//#endif // EXTERNAL_FUNCTION_H
