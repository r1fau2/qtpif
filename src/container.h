//#ifndef CONTAINER_H
//#define CONTAINER_H

#pragma once

#include <QList>
#include "base_operator.h"
#include <QMap>

class listArea;
class arrowItem;
class container {
public:

    static container* instance(){
        if(!m_this)
            m_this = new container();
        return m_this;
    }

    QList<baseOperator*> listItem;
    //QMap<int,baseOperator*> listAreas;
    QList<listArea*> listAreas;
    QMap<int, QList<baseOperator*>> areaContains;
    //добавить QMAP для областей. делать проверку, если номер области,в которую вложена текущая, присутствует там, то менять цвет(темнее)
    //если нет, то делать defaul color и смотреть. Проверять вложенность областей друг в друга, короче
    QList<arrowItem*> listArrows;
    static bool sortAreaList(listArea *second, listArea *first);
    void clearListItem();
private:
    static container *m_this;
    container(){}
    //  ~container(){}
    //container(container const&) = delete;
    // container& operator = (container const&) = delete;
};

//#endif
