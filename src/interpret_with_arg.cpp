#include "interpret_with_arg.h"
#include <QInputDialog>
#include <QtWidgets>


interpretWithArg::interpretWithArg()
{
    m_value = 0;
    m_type = type_interpreter_arg;
    m_color =QColor(225,52,28);
    addLinksPoints(QPoint(-30,0));
    addLinksPoints(QPoint(0,-30));
    addLinksPoints(QPoint(30,0));
    addLinksPoints(QPoint(0,30));
    addPresenceLink(false);
    addPresenceLink(false);
    addPresenceLink(false);
    addPresenceLink(false);
    setMaxInLink(1);
    setMaxOutLink(1);

}

interpretWithArg::~interpretWithArg()
{

}

QRectF interpretWithArg::boundingRect() const
{
    return QRectF (-30,-30,60,60);
}

void interpretWithArg::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{

    QColor fillColor = (option->state & QStyle::State_Selected) ? m_color.darker(150) : m_color;
    if (option->state & QStyle::State_MouseOver)
        fillColor = fillColor.lighter(125);

    painter->setRenderHint(QPainter::Antialiasing,true);
    painter->setPen(Qt::black);
    painter->setBrush(fillColor);
    painter->drawRect(-30,-30,60,60);
    painter->drawText(this->boundingRect(),Qt::AlignCenter,QString::number(m_value));

    Q_UNUSED(option);
    Q_UNUSED(widget);
}

void interpretWithArg::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsObject::mouseMoveEvent(event);
    update();
    updateArrows();
}

void interpretWithArg::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    //если не левая клавиша
    if (event->buttons() != Qt::LeftButton)
        return;

    QGraphicsItem::mousePressEvent(event);
    this->setCursor(QCursor(Qt::ClosedHandCursor));
    Q_UNUSED(event);
}

void interpretWithArg::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{

    this->setCursor(QCursor(Qt::ArrowCursor));
    QGraphicsItem::mouseReleaseEvent(event);
    update();
}

void interpretWithArg::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{

    if (event->buttons() != Qt::LeftButton)
        return;
    bool ok; QString str;
    int value = QInputDialog::getInt(NULL, tr("Константа:"),"Значение:"  , m_value, -2147483647, 2147483647, 1, &ok, Qt::Sheet);

    if(ok)
        m_value = value;
}

void interpretWithArg::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    scene()->clearSelection();
    setSelected(true);
    getContextMenu()->exec(event->screenPos());
}

QPixmap interpretWithArg::image()
{
    QPixmap pixmap(100,100);
    pixmap.fill(Qt::transparent);
    QPainter painter(&pixmap);
    painter.setRenderHint(QPainter::Antialiasing,true);
    painter.setPen(QPen(Qt::black,4));
    painter.setBrush(QColor(225,52,28));
    painter.translate(50, 50);
    painter.drawRect(-30,-30,60,60);
    return pixmap;
}

void interpretWithArg::setTypeOperator(int type)
{
    baseOperator::setTypeOperator(type);
}

int interpretWithArg::getTypeOperator()
{
    return baseOperator::getTypeOperator();
}

void interpretWithArg::saveToXML(QDomElement &cn)
{
    baseOperator::saveToXML(cn);
    cn.setAttribute("value", QString::number(m_value));
}

void interpretWithArg::loadFromXML(QDomElement &cn)
{
    baseOperator::loadFromXML(cn);
    m_value = cn.attribute("value").toInt();
}

int interpretWithArg::getValue()
{
    return m_value;
}

