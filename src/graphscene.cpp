#include <QGraphicsSceneHoverEvent>
#include <QMessageBox>
#include <QFile>
#include <QTextStream>
#include <QTextCodec>
#include <QDir>

#include "graphscene.h"
#include "constant.h"
#include "entry_function.h"
#include "exit_from_function.h"
#include "copy_operator.h"
#include "external_function.h"
#include "identifier.h"
#include "interpret_with_func_introduced.h"
#include "interpret_with_arg.h"
#include "interpret_arg_func_introduced.h"
#include "interpreter.h"
#include "operator_group_list.h"
#include "operator_group_parallel_list.h"
#include "mainwindow.h"
#include "container.h"
#include "list_area.h"
#include "arrow_item.h"
#include "constantFunc.h"
#include "constantFunc.h"
#include "cmath"

GraphScene::GraphScene(QMenu *itemMenu, MainWindow *parent)
: QGraphicsScene(parent)
{
    myMode = None;
    id_createItem = -1;  
    line = 0;
    m_parent = parent;
    m_ItemMenu = itemMenu;
    
    layout = new QGraphicsGridLayout;
    QGraphicsWidget *widget = new QGraphicsWidget;
    widget->setLayout(layout);
    this->addItem(widget);
    
    i = 0;
    j = 0;

    //test function  = 16.1555
    //qDebug()<< distToPoint(QPointF(-5,4), QPointF(10,10));
    connect(this, SIGNAL(checkChanged()), this, SLOT(areasUpdate()));
}



void GraphScene::setIdItemCreate(int id)
{
    id_createItem = id;
}

int GraphScene::getIdItem()
{
    return id_createItem;
}

void GraphScene::setMyMode(GraphScene::Mode m)
{
    myMode = m;
}

void GraphScene::updateScene()
{
    this->update();
}

QMenu *GraphScene::getItemContextMenu()
{
    return m_ItemMenu;
}

void GraphScene::resetScene()
{
    myMode = None;
    id_createItem = -1;
    line = 0;
}

bool GraphScene::setArrowHead(arrowItem *arrow, baseOperator *startItem, baseOperator *endItem)
{
    int type = startItem->getTypeOperator();
    switch (type)
    {
            case type_entryFunction:
            {
                for(int i = 0; i < endItem->getCountArrowIn(); i++)
                {
                    if(endItem->getArrowIn(i)->getArrowType() == arrowItem::arrow)
                        return false;
                }
                break;
            }

            case type_groupList:
            {
                for(int i = 0; i < endItem->getCountArrowIn(); i++)
                {
                    if(endItem->getArrowIn(i)->getArrowType() == arrowItem::arrow)
                        return false;
                }
                break;
            }

            case type_groupParList:
            {
                for(int i = 0; i < endItem->getCountArrowIn(); i++)
                {
                    if( endItem->getArrowIn(i)->getArrowType() == arrowItem::circle)
                        return false;
                }
                arrow->setArrowHeadFunc(true);
                break;
            }

            case type_interpreter:
            {
                for(int i = 0; i < endItem->getCountArrowIn(); i++)
                {
                    if(endItem->getArrowIn(i)->getArrowType() == arrowItem::arrow)
                        return false;
                }
                break;
            }

            case type_interpret_func:
            {
                for(int i = 0; i < endItem->getCountArrowIn(); i++)
                {
                    if(endItem->getArrowIn(i)->getArrowType() == arrowItem::arrow)
                        return false;
                }
                break;
            }

            case type_interpreter_arg:
            {
                for(int i = 0; i < endItem->getCountArrowIn(); i++)
                {
                    if(endItem->getArrowIn(i)->getArrowType() == arrowItem::arrow)
                        return false;
                }
                break;
            }

            case type_interpret_arg_func:
            {
                for(int i = 0; i < endItem->getCountArrowIn(); i++)
                {
                    if(endItem->getArrowIn(i)->getArrowType() == arrowItem::arrow)
                        return false;
                }
                break;
            }

            case type_specSymbol:
            {
                for(int i = 0; i < endItem->getCountArrowIn(); i++)
                {
                    if( endItem->getArrowIn(i)->getArrowType() == arrowItem::circle)
                        return false;
                }
                arrow->setArrowHeadFunc(true);
                break;
            }

            case type_constant:
            {
                for(int i = 0; i < endItem->getCountArrowIn(); i++)
                {
                    if(endItem->getArrowIn(i)->getArrowType() == arrowItem::arrow)
                        return false;
                }
                break;
            }

            case type_constantFunc:
            {
                for(int i = 0; i < endItem->getCountArrowIn(); i++)
                {
                    if( endItem->getArrowIn(i)->getArrowType() == arrowItem::circle)
                        return false;
                }
                arrow->setArrowHeadFunc(true);
                break;
            }

            case type_external_function:
            {
                for(int i = 0; i < endItem->getCountArrowIn(); i++)
                {
                    if( endItem->getArrowIn(i)->getArrowType() == arrowItem::circle)
                        return false;
                }
                arrow->setArrowHeadFunc(true);
                break;
            }

            case type_identifier:
            {
                for(int i = 0; i < endItem->getCountArrowIn(); i++)
                {
                    if(endItem->getArrowIn(i)->getArrowType() == arrowItem::arrow)
                        return false;
                }
                break;
            }

            case type_copy_operator:
            {

                for(int i = 0; i < endItem->getCountArrowIn(); i++)
                {
                    if(endItem->getArrowIn(i)->getArrowType() == arrowItem::arrow)
                        return false;
                }
                break;
            }

            default:
                return true;
        }
    return true;
}

bool GraphScene::checkLink(baseOperator *start, baseOperator *end, arrowItem *arrow)
{
    bool y = true;
    bool n = false;
    bool link[15][15] = {
      //0   1   2   3   4   5   6   7   8   9   10  11  12  13  14
/*0*/   n,  n,  n,  y,  n,  y,  y,  n,  n,  y,  y,  y,  y,  n,  n,
/*1*/   n,  n,  n,  n,  n,  n,  n,  n,  n,  n,  n,  n,  n,  n,  n,
/*2*/   n,  n,  n,  y,  n,  y,  y,  n,  n,  y,  y,  y,  y,  n,  n,
/*3*/   n,	y,	n,	n,	y,	y,	y,	n,	n,	y,	y,	y,	n,	n,	n,
/*4*/   n,	n,	n,	y,	n,	y,	y,	y,	n,	n,	y,	y,	n,	n,	n,
/*5*/   n,	y,	n,	y,	n,	n,	y,	y,	n,	y,	y,	y,	y,	n,	n,
/*6*/   n,	n,	n,	y,	n,	y,	y,	n,	n,	y,	y,	y,	y,	n,	n,
/*7*/   n,	y,	n,	y,	n,	y,	y,	n,	n,	y,	y,	y,	y,	n,	n,
/*8*/   n,	n,	n,	y,	n,	y,	y,	n,	n,	y,	y,	y,	y,	n,	n,
/*9*/   n,	y,	n,	y,	n,	y,	y,	y,	n,	n,	y,	y,	y,	n,	n,
/*10*/  n,	y,	n,	y,	n,	y,	y,	y,	n,	y,	n,	y,	y,	n,	n,
/*11*/  n,	y,	n,	y,	n,	y,	y,	y,	n,	y,	y,	n,	y,	n,	n,
/*12*/  n,	y,	n,	y,	n,	y,	y,	y,	n,	y,	y,	y,	n,	n,	n,
/*13*/  n,	n,	n,	n,	n,	n,	n,	n,	n,	n,	n,	n,	n,	n,	n,
/*14*/  n,	n,	n,	y,	n,	y,	y,	n,	n,	y,	y,	y,	y,	n,	n,
    };

    if(link[(int)start->getTypeOperator()][(int)end->getTypeOperator()] == true)
    {
        if(start->getTypeOperator() != type_copy_operator)
            if(end->getTypeOperator() == type_interpreter || end->getTypeOperator() == type_interpreter_arg)
            {
              if(setArrowHead(arrow, start, end))
                    return true;

                qDebug() << "Нельзя добавить второй вход функции!";
                return false;
            }

        if(end->getTypeOperator() == type_copy_operator)
        {
            copyOperator *c = qgraphicsitem_cast<copyOperator*>(end);
            c->setOperator(start);
            qDebug();
        }

        if(start->getTypeOperator() == type_copy_operator)
        {
            copyOperator *c = qgraphicsitem_cast<copyOperator*>(start);
            qDebug()<<c->getOperator()->getTypeOperator() << " -  " << end->getTypeOperator();

            if(c->getOperator() != 0)
            {
                if(link[c->getOperator()->getTypeOperator()][end->getTypeOperator()])
                {

                    if(end->getTypeOperator() == type_interpreter || end->getTypeOperator() == type_interpreter_arg)
                    {
                      if(setArrowHead(arrow, c->getOperator(), end))
                            return true;

                        qDebug() << "Нельзя добавить второй вход функции!";
                        return false;
                    }

                }
                return true;
            }

            return false;
        }

        return true;
    }
    return false;
}

bool GraphScene::setLinePointAtItem(baseOperator *start, baseOperator *end, const QPointF startPoint, const QPointF endPoint, arrowItem *arrow)
{
    if(start->getTypeOperator() == type_identifier || end->getTypeOperator() == type_identifier)
    {
        baseOperator *op;
        if(start->getTypeOperator() == type_identifier)
            op = start;

        if(end->getTypeOperator() == type_identifier)
            op = end;

        if(op->getCurrentInLink() == 1 || op->getCurrentOutLink() == 1)
            return false;
    }

    if((start->getMaxOutLink() != start->getCurrentOutLink()
        && start->getMaxOutLink()!=0) &&
       (end->getMaxInLink() != end->getCurrentInLink()
        && end->getMaxInLink()!=0))
    {
        int startIndex = -1;
        bool fl = false; // для присваивания первой доступной точки статуса ближайшей
        float min_value;

        for(int i=0; i<start->getCountLinksPoints(); i++)
        {
            float current_val;
            if(start->getPresenceLink(i) == false)
            {
                if(fl == false)
                {
                     min_value = distToPoint(startPoint,start->getLinkPoint(i));
                     startIndex = i;
                     fl = true;
                }

                if(fl == true)
                {
                    current_val = distToPoint(startPoint,start->getLinkPoint(i));
                    if( current_val < min_value )
                    {
                        min_value = current_val;
                        startIndex = i;
                    }
                }
             }
        }

        int endIndex = -1;
        fl = false;
        for(int i=0; i<end->getCountLinksPoints(); i++)
        {
            float current_val = 0;
            if(end->getPresenceLink(i) == false)
            {
                if(fl == false)
                {
                     min_value = distToPoint(endPoint,end->getLinkPoint(i));
                     endIndex = i;
                     fl = true;
                }

                if(fl == true)
                {
                    current_val = distToPoint(endPoint,end->getLinkPoint(i));
                    if( current_val < min_value )
                    {
                        min_value = current_val;
                        endIndex = i;
                    }
                }
             }
          }

        arrow->setStartPoint(start->getLinkPoint(startIndex));
        arrow->setEndPoint(end->getLinkPoint(endIndex));

        if(start->getTypeOperator() != type_copy_operator && start->getTypeOperator() != type_entryFunction)
            start->setPresenceLink(true, startIndex);

        if(end->getTypeOperator() != type_groupList && end->getTypeOperator() != type_groupParList)
                end->setPresenceLink(true, endIndex);


        arrow->setStartPointNum(startIndex);
        arrow->setEndPointNum(endIndex);

//        start->setCurrentOutLink(start->getCurrentOutLink()+1);//реализовано при добавлении
//        end->setCurrentInLink(end->getCurrentInLink()+1);      //стрелки в класс

        return true;
    }
    else
    {
        qDebug() << "Нельзя добавить связь!!!!";
        return false;
    }
}

float GraphScene::distToPoint(const QPointF start, const QPointF end)
{
    return sqrt(pow((end.x()-start.x()),2) + pow((end.y()-start.y()),2));
}

void GraphScene::writeLink(QTextStream &stream, QList<baseOperator *> *listConst, QList<baseOperator *> *listFunc, baseOperator *op)
{
    QList<arrowItem*> arrows;
    QList<baseOperator*> opList;
    for( int i = 0; i < op->getCurrentInLink(); i++)
    {
        arrows.append(op->getArrowIn(i));
        opList.append(op->getArrowIn(i)->getStartItem());
    }

    foreach (baseOperator *obj, opList)
    {
        TypeOperator type = (TypeOperator)obj->getTypeOperator();
        // свитч и проверка оператора,  пробег по массивам если константа или функция + после этого доб проверку на кол-во связей, чтоб выкидывать ошибку
        switch (type)
        {
                case type_entryFunction:
                {
                    stream << obj->getId() << " ";// arg
                    break;
                }

                case type_groupList:
                {
                    stream << obj->getId() << " ";
                    break;
                }

                case type_groupParList:
                {
                    stream << obj->getId() << " ";
                    break;
                }

                case type_interpreter:
                {
                    stream << obj->getId() << " ";
                    break;
                }

                case type_interpret_func:
                {
                    int i = 0;
                    foreach (baseOperator *func, *listFunc)
                    {
                        if(func == obj)
                        {
                            stream << "ext:" << i << " ";
                            break;
                        }
                        i++;
                    }
                    break;
                }

                case type_interpreter_arg:
                {
                    int i = 0;
                    foreach (baseOperator *func, *listConst)
                    {
                        if(func == obj)
                        {
                            stream << "loc:" << i << " ";
                            break;
                        }
                        i++;
                    }

                    i = 0;
                    foreach (baseOperator *func, *listFunc)
                    {
                        if(func == obj)
                        {
                            stream << "ext:" << i << " ";
                            break;
                        }
                        i++;
                    }
                    break;
                }

                case type_interpret_arg_func:
                {
                    int i = 0;
                    foreach (baseOperator *func, *listFunc)
                    {
                        if(func == obj)
                        {
                            stream << "loc:" << i << " ";
                            break;
                        }
                        i++;
                    }
                    break;
                }

                case type_specSymbol:
                {
                    stream << obj->getId() << " ";

                    break;
                }

                case type_constant:
                {
                    int i = 0;
                    foreach (baseOperator *func, *listConst)
                    {
                        if(func == obj)
                        {
                            stream << "loc:" << i << " ";
                            break;
                        }
                        i++;
                    }
                    break;
                }

                case type_constantFunc:
                {
                    constantFunc *c = qgraphicsitem_cast<constantFunc*>(obj);
                    stream << c->getValue() << " ";
                    //stream << obj->getId() << " ";
                    break;
                }

                case type_external_function:
                {
                    int i = 0;
                    foreach (baseOperator *func, *listFunc)
                    {
                        if(func == obj)
                        {
                            stream << "ext:" << i << " ";
                            break;
                        }
                        i++;
                    }
                    break;
                }

                case type_identifier:
                {

                    break;
                }

                case type_copy_operator:
                {
                    copyOperator *c = qgraphicsitem_cast<copyOperator*>(obj);
                    writeLink_copyOp(stream, listConst, listFunc, c->getOperator());
                    break;
                }

                default:
                    break;
            }
    }
}

void GraphScene::writeLink_copyOp(QTextStream &stream, QList<baseOperator *> *listConst, QList<baseOperator *> *listFunc, baseOperator *obj)
{
        TypeOperator type = (TypeOperator)obj->getTypeOperator();
        // свитч и проверка оператора,  пробег по массивам если константа или функция + после этого доб проверку на кол-во связей, чтоб выкидывать ошибку
        switch (type)
        {
                case type_entryFunction:
                {
                    stream << obj->getId() << " ";// arg
                    break;
                }

                case type_groupList:
                {
                    stream << obj->getId() << " ";
                    break;
                }

                case type_groupParList:
                {
                    stream << obj->getId() << " ";
                    break;
                }

                case type_interpreter:
                {
                    stream << obj->getId() << " ";
                    break;
                }

                case type_interpret_func:
                {
                    int i = 0;
                    foreach (baseOperator *func, *listFunc)
                    {
                        if(func == obj)
                        {
                            stream << "ext:" << i << " ";
                            break;
                        }
                        i++;
                    }
                    break;
                }

                case type_interpreter_arg:
                {
                    int i = 0;
                    foreach (baseOperator *func, *listConst)
                    {
                        if(func == obj)
                        {
                            stream << "loc:" << i << " ";
                            break;
                        }
                        i++;
                    }

                    i = 0;
                    foreach (baseOperator *func, *listFunc)
                    {
                        if(func == obj)
                        {
                            stream << "ext:" << i << " ";
                            break;
                        }
                        i++;
                    }
                    break;
                }

                case type_interpret_arg_func:
                {
                    int i = 0;
                    foreach (baseOperator *func, *listFunc)
                    {
                        if(func == obj)
                        {
                            stream << "loc:" << i << " ";
                            break;
                        }
                        i++;
                    }
                    break;
                }

                case type_specSymbol:
                {
                    stream << obj->getId() << " ";

                    break;
                }

                case type_constant:
                {
                    int i = 0;
                    foreach (baseOperator *func, *listConst)
                    {
                        if(func == obj)
                        {
                            stream << "loc:" << i << " ";
                            break;
                        }
                        i++;
                    }
                    break;
                }

                case type_constantFunc:
                {
                    constantFunc *c = qgraphicsitem_cast<constantFunc*>(obj);
                    stream << c->getValue() << " ";
                    //stream << obj->getId() << " ";
                    break;
                }

                case type_external_function:
                {
                    int i = 0;
                    foreach (baseOperator *func, *listFunc)
                    {
                        if(func == obj)
                        {
                            stream << "ext:" << i << " ";
                            break;
                        }
                        i++;
                    }
                    break;
                }

                case type_identifier:
                {

                    break;
                }
                default:
                    break;
        }
}

void GraphScene::generateRIG()
{
    bool entry = false;
    bool exit = false;
    int indexOperator;
    int indexEntry = 0;
    int step = 0;
    container *cont = container::instance();
    foreach (baseOperator *obj, cont->listItem) {
        if(obj->getTypeOperator() == type_entryFunction)
        {
            if(entry == true)
            {
                QMessageBox::warning(NULL, tr("Ошибка"), tr("Слишком много узлов входа в функцию"));
                return;
            }
            entry = true;
            indexEntry = step;
        }

        if(obj->getTypeOperator() == type_exitFunction)
        {
            if(exit == true)
            {
                QMessageBox::warning(NULL, tr("Ошибка"), tr("Узлов выхода больше одного"));
                return;
            }
            exit = true;
            indexOperator = step;
        }
        step++;
    }
        if(entry == false || exit == false)
        {
            QMessageBox::critical(NULL, tr("Ошибка"), tr("Недостаточно узлов"));
            return;
        }

        //вход в началао
        //выход в конец


        cont->listItem.append(cont->listItem.at(indexOperator));
        cont->listItem.removeAt(indexOperator);
        step = 0;
        while(step != 3)
        {
            if(indexEntry == 0)
                break;

            if(cont->listItem.at(indexEntry-1 + step)->getTypeOperator() == type_entryFunction)
            {
                baseOperator *op = cont->listItem.at(indexEntry-1 + step);
                cont->listItem.removeAt(indexEntry-1 + step);
                cont->listItem.prepend(op);
                break;
            }
            step++;
        }

        step = 0;
        foreach (baseOperator *obj, cont->listItem)
        {
            if(obj->getTypeOperator() == type_constant || obj->getTypeOperator() == type_copy_operator || obj->getTypeOperator() == type_external_function ||
               obj->getTypeOperator() == type_identifier || obj->getTypeOperator() == type_constantFunc || obj->getTypeOperator() == type_listArea)
            {
                cont->listItem.removeAt(step);
                cont->listItem.prepend(obj);
            }
            step++;

        }

        bool fl = false;
        step = 0;
        foreach (baseOperator *obj, cont->listItem)
        {
            if(obj->getTypeOperator() == type_entryFunction)
            {
                fl = true;
                indexEntry = cont->listItem.indexOf(obj);
            }

            if(fl)
            {
                obj->setId(step);
                step++;
            }
            if(!fl)
                obj->setId(-1);
        }

        updateDelayItem();//обновление для изменения присваивания delay

        QList<baseOperator*> constantList;
        QList<baseOperator*> externalFuncList;

        foreach (baseOperator *obj, cont->listItem)
        {
                switch (obj->getTypeOperator()) {
                case type_constant:
                    constantList.append(obj);
                    break;
                case type_interpreter_arg:
                    constantList.append(obj);
                    break;
                case type_interpret_arg_func:
                    constantList.append(obj);
                    externalFuncList.append(obj);
                    break;
                case type_external_function:
                    externalFuncList.append(obj);
                    break;
                case type_interpret_func:
                    externalFuncList.append(obj);
                    break;
                default:
                    break;
            }
        }
        qDebug()<<"Операции выполнены";

        QTextCodec::setCodecForLocale( QTextCodec::codecForName( "UTF-8" ) );

        QFile out( QDir::homePath()+"/file.rig" );
            if( !out.open( QIODevice::WriteOnly ) )
            {
                qDebug()<<"Не удалось записать файл!";
                return;
            }

            foreach (baseOperator *op, cont->listItem) {
                qDebug()<< op->getId() << " ";
            }

    QTextStream stream( &out );
    stream.setCodec( "UTF-8 ");


    //пишем внешние функции
            stream << "External" << endl;
             int id = -1;
            foreach (baseOperator *obj, externalFuncList)
            {
                int delay = -1;
                QString name;
                TypeOperator type = (TypeOperator)obj->getTypeOperator();
                switch (type)
                {
                    case type_interpret_arg_func:
                {
                        interpretArgFuncIntroduced *o = qgraphicsitem_cast<interpretArgFuncIntroduced*>(obj);
                        id++;
                        delay = o->getDelayNumber();
                        name = o->getFuncName();
                    break;
                }
                case type_external_function:
                {
                        external_function *o = qgraphicsitem_cast<external_function*>(obj);
                        id++;
                        delay = o->getDelayNumber();
                        name = o->getFuncName();
                        break;
                }
                case type_interpret_func:
                {
                        interpretWithFuncIntroduced *o = qgraphicsitem_cast<interpretWithFuncIntroduced*>(obj);
                        id++;
                        delay = o->getDelayNumber();
                        name = o->getFuncName();
                        break;
                }

                    default:
                        break;
                }
                stream << "\t" << id;
                if(delay != 0)
                    stream << "\t" << "{" << delay << "}" << name << endl;
                else
                    stream << "\t" << name << endl;

            }

           //пишем константы
            stream << endl << "Local" << endl;
            id = -1;
            foreach (baseOperator *obj, constantList)
            {
                int value = 0;
                int delay = -1;
                id++;
                TypeOperator type = (TypeOperator)obj->getTypeOperator();
                switch (type)
                {

                    case type_constant:
                    {

                        Constant *o = qgraphicsitem_cast<Constant*>(obj);
                        value = o->getValue();
                        delay = o->getDelayNumber();
                        break;
                    }
                    case type_interpreter_arg:
                    {
                        interpretWithArg *o = qgraphicsitem_cast<interpretWithArg*>(obj);

                        value = o->getValue();
                        delay = o->getDelayNumber();
                        break;
                    }
                    case type_interpret_arg_func:
                    {
                        interpretArgFuncIntroduced *o = qgraphicsitem_cast<interpretArgFuncIntroduced*>(obj);

                        value = o->getValue();
                        delay = o->getDelayNumber();
                        break;
                    }

                    default:
                        break;
                }

                    stream << "\t" << id;
                    if(delay != 0)
                        stream << "\t" << "{" << delay << "}" << value << endl;
                    else
                        stream << "\t" << value << endl;
                }
                // вывод операций

                stream << "id\t" << "delay\t" << "operation\t" << "links" << "\t\tposition" << endl;
                step = 0;

                foreach (baseOperator *obj, cont->listItem)
                {
                    TypeOperator type = (TypeOperator)obj->getTypeOperator();
                    if(obj->getId() >= 0)
                        switch (type)
                        {
                            case type_entryFunction:
                            {
                                stream << obj->getId() << "\t" <<  obj->getDelayNumber() << "\t\t" << "arg" << "\t" << "0, 0, 0, 0"<< endl;
                                break;
                            }

                            case type_groupList:
                            {
                                stream << obj->getId() << "\t" <<  obj->getDelayNumber() << "\t\t" << "(---)\t\t";
                                writeLink(stream, &constantList, &externalFuncList, obj);
                                stream << "\t\t\t" << "0, 0, 0, 0";
                                stream << endl;
                                break;
                            }

                            case type_groupParList:
                            {
                                stream << obj->getId() << "\t" <<  obj->getDelayNumber() << "\t\t" << "[---]\t\t";
                                writeLink(stream, &constantList, &externalFuncList, obj);
                                stream << "\t\t\t" << "0, 0, 0, 0";
                                stream << endl;
                                break;
                            }

                            case type_interpreter:
                            {
                                stream << obj->getId() << "\t" <<  obj->getDelayNumber() << "\t\t" << ":\t\t\t";
                                writeLink(stream, &constantList, &externalFuncList, obj);
                                stream << "\t\t\t" << "0, 0, 0, 0";
                                stream << endl;
                                break;
                            }

                            case type_interpret_func:
                            {
                                stream << obj->getId() << "\t" <<  obj->getDelayNumber() << "\t\t" << ":\t\t\t";
                                writeLink(stream, &constantList, &externalFuncList, obj);
                                stream << "\t\t\t" << "0, 0, 0, 0";
                                stream << endl;
                                break;
                            }

                            case type_interpreter_arg:
                            {
                                stream << obj->getId() << "\t" <<  obj->getDelayNumber() << "\t\t" << ":\t\t\t";
                                writeLink(stream, &constantList, &externalFuncList, obj);
                                stream << "\t\t\t" << "0, 0, 0, 0";
                                stream << endl;
                                break;
                            }

                            case type_interpret_arg_func:
                            {
                                stream << obj->getId() << "\t" <<  obj->getDelayNumber() << "\t\t" << ":\t\t\t";
                                writeLink(stream, &constantList, &externalFuncList, obj);
                                stream << "\t\t\t" << "0, 0, 0, 0";
                                stream << endl;
                                break;
                            }

                            case type_specSymbol:
                            {
                                stream << obj->getId() << "\t" <<  obj->getDelayNumber() << "\t\t" << ":\t\t\t";
                                writeLink(stream, &constantList, &externalFuncList, obj);
                                specSymbol *symb = qgraphicsitem_cast<specSymbol*>(obj);
                                stream << symb->getValue() << " ";
                                stream << "\t\t\t" << "0, 0, 0, 0";
                                stream << endl;
                                break;
                            }

                            case type_exitFunction:
                            {
                                stream << obj->getId() << "\t" <<  obj->getDelayNumber() << "\t\t" << "return\t\t";
                                writeLink(stream, &constantList, &externalFuncList, obj);
                                stream << "\t\t\t" << "0, 0, 0, 0";
                                break;
                            }

                            default:
                                break;
                            }
                }
    out.close();
}

void GraphScene::updateDelayItem()
{
    container *cont = container::instance();

    if(cont->listItem.size() == 0 || cont->listAreas.size() == 0)
        return;

    foreach (baseOperator *item, cont->listItem)
    {
        if(item->getTypeOperator() != type_listArea)
            item->setDelayNumber(0);
    }


    for(int i = cont->listAreas.size()-1 ; i >= 0; i--)
    {
        baseOperator *checkDelay = cont->listAreas.at(i);
        QList<QGraphicsItem*> list = collidingItems(checkDelay);

        if(list.size() != 0)
            for(int index = list.size()-1; index >= 0; index--)
            {
                QGraphicsItem *obj = list.at(index);

                if(obj->zValue() != 5000)//отбрасываем стрелки
                {
                    baseOperator *bufItem;
                    bufItem = qgraphicsitem_cast<baseOperator*>(obj);
                    if(bufItem->getTypeOperator() != type_listArea && bufItem->getDelayNumber() == 0)
                    {
                        bufItem->setDelayNumber(checkDelay->getDelayNumber());
                    }
                }
            }
     }
}

void GraphScene::areasUpdate()
{
    container *cont = container::instance();
    if(cont->listAreas.size() <= 1)
        return;


    QColor baseColor(QColor(240,240,240));
    baseOperator *bufItem;
    int zAreaValue = -5000;
    int *i = new int(1);
    foreach (listArea *checkItem, cont->listAreas) {
        checkItem->m_color = baseColor;
        checkItem->setZValue(zAreaValue);
        zAreaValue++;
        checkItem->setDelayNumber(*i);
        *i = *i+1;
    }
    delete i;

    for(int i = 0; i<cont->listAreas.size(); i++)
    {
        baseOperator *checkItem = cont->listAreas.at(i);
        QList<QGraphicsItem*> list = collidingItems(checkItem);
        foreach (QGraphicsItem *obj, list)
        {
            if(obj->zValue() != 5000)//отбрасываем стрелки
            {
                bufItem = dynamic_cast<baseOperator*>(obj);

                if(bufItem->getTypeOperator() == type_listArea)
                {

                    qDebug()<<"Z: "<<bufItem->getZ() <<" -  "<< checkItem->getZ();

                    if(bufItem->getZ() > checkItem->getZ())
                        bufItem->m_color = checkItem->m_color.darker(110);
                }
            }
        }
    }
}

void GraphScene::keyPressEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Backspace) {
        // remove all selected items
        qDebug() << "выделено элементов" << selectedItems().size();
        while(!selectedItems().isEmpty()) {
            removeItem(selectedItems().front());
        }
    } else {
        QGraphicsScene::keyPressEvent(event);
    }
}

void GraphScene::mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    if (mouseEvent->button() != Qt::LeftButton)
    {
        //пока что
        mouseEvent->ignore();
        return;
    }

    baseOperator *op = NULL;

 switch (myMode)
    {
    case InsertItem:
    {
        container *cont = container::instance();
        op = operator_factory::createItem(this, id_createItem);
        op->setPos(mouseEvent->scenePos());
        this->addItem(op);
        //layout->addItem(op, this->i++, this->j++, Qt::AlignRight);
        
        op->update();
        qDebug()<<"Mouse pos(): "<<mouseEvent->pos()<<endl;
        qDebug()<<"Mouse ScenePos(): "<<mouseEvent->scenePos()<<endl;
        cont->listItem.append(op);
        //if(op->getTypeOperator() == type_identifier)

        op->setContextMenu(m_ItemMenu);
        myMode = MoveItem;
        emit releaseButtOperator(id_createItem);
        id_createItem = -1;
        m_parent->contentChanged(true);
        
        //this->update();
        //view->update();
        
        break;
    }

    case InsertArea:
    {
        container *cont = container::instance();
        op = new listArea(this);
        op->setContextMenu(m_ItemMenu);
        listArea *arlist = dynamic_cast<listArea*>(op);
        op->setPos(mouseEvent->scenePos());

        m_selected = op;
        arlist->m_top = mouseEvent->scenePos();
        op->update();
        cont->listItem.append(op);
        //добавление area
        emit releaseButtOperator(id_createItem);

        id_createItem = -1;
        break;
    }

    case InsertLine: {
         line = new QGraphicsLineItem(QLineF(mouseEvent->scenePos(),
                                             mouseEvent->scenePos()));
         line->setPen(QPen(Qt::black, 2));
         addItem(line);

         QGraphicsScene::mousePressEvent(mouseEvent);
         m_startLinePoint =mouseEvent->pos();
         QGraphicsScene::mouseReleaseEvent(mouseEvent);
         qDebug()<<"Кликнуто в: "<< m_startLinePoint;
         break;
    }

    case MoveItem:
        QGraphicsScene::mousePressEvent(mouseEvent);
        break;
    default:
        break;
    }
}

void GraphScene::mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    if (myMode == InsertLine && line != 0)
    {
        QLineF newLine(line->line().p1(), mouseEvent->scenePos());
        line->setLine(newLine);
    }
    else if (myMode == MoveItem)
    {
        QGraphicsScene::mouseMoveEvent(mouseEvent);
        if(selectedItems().size() >= 1)
            m_parent->contentChanged(true);

        if(selectedItems().size() > 1)
        {
            baseOperator *op;
            QList<QGraphicsItem*> items = selectedItems();

            foreach(QGraphicsItem *currentItem, items)
            {
              if(currentItem->zValue() != 5000 && currentItem->zValue() >= 0)
              {
                op = qgraphicsitem_cast<baseOperator*>(currentItem);
                    op->updateArrows();
              }
            }
        }
    }
}

void GraphScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    switch(myMode)
    {
    case InsertArea:
    {
        QPainterPath rec = selectionArea();
        listArea *arlist = dynamic_cast<listArea*>(m_selected);
        arlist->m_downRight = mouseEvent->scenePos();
        myMode = MoveItem;
        arlist->m_width = abs(arlist->m_downRight.x() - arlist->m_top.x());
        arlist->m_height = abs(arlist->m_downRight.y() - arlist->m_top.y());
        container *cont = container::instance();

        cont->listAreas.append(dynamic_cast<listArea*>(arlist));
        std::sort(cont->listAreas.begin(), cont->listAreas.end(), cont->sortAreaList);
        this->addItem(arlist);
        //layout->addItem(arlist, 1, 2);

        areasUpdate();
        m_parent->contentChanged(true);
        break;
    }
    case MoveItem:
        QGraphicsScene::mouseReleaseEvent(mouseEvent);
        break;
    case InsertLine:
    {

        QGraphicsScene::mouseReleaseEvent(mouseEvent);

      if(line!=0)
      {
          QList<QGraphicsItem *> startItems = items(line->line().p1());
          if (startItems.count() && startItems.first() == line)
              startItems.removeFirst();
          QList<QGraphicsItem *> endItems = items(line->line().p2());
          if (endItems.count() && endItems.first() == line)
              endItems.removeFirst();

          removeItem(line);
          delete line;

          QGraphicsScene::mouseReleaseEvent(mouseEvent);


          if (startItems.count() > 0 && endItems.count() > 0 && startItems.first() != endItems.first()
                  && startItems.first()->zValue() != 5000 && endItems.first()->zValue() != 5000)
          {

              baseOperator *startItem = dynamic_cast<baseOperator *>(startItems.first());
              baseOperator *endItem = dynamic_cast<baseOperator *>(endItems.first());
              arrowItem *arrow = new arrowItem(startItem, endItem);

              QGraphicsScene::mousePressEvent(mouseEvent);
              m_endLinePoint = mouseEvent->pos();
              QGraphicsScene::mouseReleaseEvent(mouseEvent);
                if(checkLink(startItem, endItem, arrow))
                  if( setLinePointAtItem(startItem, endItem, m_startLinePoint, m_endLinePoint, arrow) )
                  {
                      startItem->addArrowOut(arrow);
                      endItem->addArrowIn(arrow);
                      arrow->setZValue(5000.0);
                      addItem(arrow);

                      container *cont = container::instance();
                      cont->listArrows.append(arrow);
                      qDebug()<<"Отжато в: "<<mouseEvent->pos();
                      clearSelection();
                      m_parent->contentChanged(true);
                  }

              qDebug()<<"Отжато в: "<<mouseEvent->pos();
          }
      }
      break;
    }

    default:
          break;

    }

      line = 0;
}
