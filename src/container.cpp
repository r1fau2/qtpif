#include "container.h"
#include "list_area.h"

container* container::m_this = 0;

//компаратор сортировки областей выделения
bool container::sortAreaList(listArea *second, listArea *first)
{

//    qDebug()<<"W1-"<<first->getBoundingRect().width()<<" H1-" << first->getBoundingRect().height()<<" и W2-" << second->getBoundingRect().width() <<" H2-" << second->getBoundingRect().height();
        if ((first->getBoundingRect().width() * first->getBoundingRect().height()) < (second->getBoundingRect().width() * second->getBoundingRect().height()))
        {
            return true;
        }
        else if ((first->getBoundingRect().width() * first->getBoundingRect().height()) > (second->getBoundingRect().width() * second->getBoundingRect().height()))
        {
            return false;
        }
        return true;
}

void container::clearListItem()
{
    foreach (baseOperator *op, listItem) {
        op->removeArrows();
        delete op;
    }
    listItem.clear();
    listAreas.clear();
    listArrows.clear();
}
