#include "operator_group_list.h"
#include <QtWidgets>


operatorGroupList::operatorGroupList()
{
    m_type = type_groupList;
    m_color = QColor(60,60,70);
    int step = 16;
    for(int i=0; i<10;i++)
    {
        addLinksPoints(QPoint(-80+step*i,0));
        addPresenceLink(false);
    }
    addLinksPoints(QPoint(0,0));
    addPresenceLink(false);
    setMaxInLink(-1);
    setMaxOutLink(1);
}

operatorGroupList::~operatorGroupList()
{

}

void operatorGroupList::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{

    QColor fillColor = (option->state & QStyle::State_Selected) ? QColor(Qt::black) : m_color;
    if (option->state & QStyle::State_MouseOver)
        fillColor = fillColor.lighter(175);

    QColor cl(QColor(60,60,70));

    painter->setRenderHint(QPainter::Antialiasing,true);
    painter->setPen(QPen(fillColor,3));


    QRectF leftRect(-90.0, -30.0, 45.0, 60.0);
    QRectF rightRect(45.0, -30.0, 45.0, 60.0);
    //левая скобка
    painter->drawArc(leftRect, 110*16, 140*16);
    //правая скобка
    painter->drawArc(rightRect, 290*16, 140*16);
    //средняя линия
    painter->drawLine(QPointF(-90,0), QPointF(90,0));


    Q_UNUSED(option);
    Q_UNUSED(widget);
}

QRectF operatorGroupList::boundingRect() const
{
    return QRectF(-90,-30,183,63);
}

void operatorGroupList::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{

    QGraphicsObject::mouseMoveEvent(event);
    update();
    updateArrows();
}

void operatorGroupList::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mousePressEvent(event);
    this->setCursor(QCursor(Qt::ClosedHandCursor));
    Q_UNUSED(event);
}

void operatorGroupList::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{

    this->setCursor(QCursor(Qt::ArrowCursor));
    QGraphicsItem::mouseReleaseEvent(event);
    update();
}

void operatorGroupList::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    scene()->clearSelection();
    setSelected(true);
    getContextMenu()->exec(event->screenPos());
}

QPixmap operatorGroupList::image()
{
    QPixmap pixmap(150,150);
    pixmap.fill(Qt::transparent);
    QPainter painter(&pixmap);
    painter.setRenderHint(QPainter::Antialiasing,true);
    painter.setPen(QPen(Qt::black,8));
    painter.setBrush(QColor(225,52,28));
    painter.translate(100, 75);

    QRectF leftRect(-90.0, -30.0, 45.0, 60.0);
    QRectF rightRect(0, -30.0, 45.0, 60.0);

    painter.drawArc(leftRect, 110*16, 140*16);
    //правая скобка
    painter.drawArc(rightRect, 290*16, 140*16);
    //средняя линия
    painter.drawLine(QPointF(-90,0), QPointF(45,0));

    return pixmap;
}

void operatorGroupList::setTypeOperator(int type)
{
    baseOperator::setTypeOperator(type);
}

int operatorGroupList::getTypeOperator()
{
    return baseOperator::getTypeOperator();
}

void operatorGroupList::saveToXML(QDomElement &cn)
{
    baseOperator::saveToXML(cn);
}

void operatorGroupList::loadFromXML(QDomElement &cn)
{
    baseOperator::loadFromXML(cn);
}

