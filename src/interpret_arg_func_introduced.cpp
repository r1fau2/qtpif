#include "interpret_arg_func_introduced.h"
#include <QDialog>
#include <QPushButton>
#include <QGraphicsSceneMouseEvent>
#include <QLineEdit>
#include <QLayout>
#include <QLabel>
#include <QSpinBox>
#include <QtWidgets>
#include "arrow_item.h"

interpretArgFuncIntroduced::interpretArgFuncIntroduced()
{
    m_constanta = 0;
    m_funcName = "";
    m_type = type_interpret_arg_func;
    m_color = QColor(225,52,28);
    setWidht(100);
    setHeight(60);
    m_widhtAdd = 0;

    oldRightPoint = rightPoint = QPoint(40,18);//18 - середина квадрата
    oldLeftPoint = leftPoint = QPoint(-40,18);
    addLinksPoints(leftPoint);
    addLinksPoints(rightPoint);
    addLinksPoints(QPoint(0,42));
    addPresenceLink(false);
    addPresenceLink(false);
    addPresenceLink(false);
    setMaxInLink(0);
    setMaxOutLink(1);

}

interpretArgFuncIntroduced::~interpretArgFuncIntroduced()
{

}

void interpretArgFuncIntroduced::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{

    QColor fillColor = (option->state & QStyle::State_Selected) ? m_color.darker(150) : m_color;
    if (option->state & QStyle::State_MouseOver)
        fillColor = fillColor.lighter(125);


    painter->setRenderHint(QPainter::Antialiasing,true);
    painter->setPen(QPen(Qt::black,2));
    painter->setBrush(fillColor);

    QRect rectHigh(-40, -60, 80, 50);
    QRect rectDown(-40-m_widhtAdd/2, -8, 80+m_widhtAdd, 50);


    painter->drawRoundedRect(rectDown, 20, 20);
    painter->drawRect(rectHigh);
    painter->drawText(rectHigh,Qt::AlignCenter, QString::number(m_constanta));
    painter->drawText(rectDown,Qt::AlignCenter, m_funcName);


    Q_UNUSED(option);
    Q_UNUSED(widget);
}

QRectF interpretArgFuncIntroduced::boundingRect() const
{
    return QRectF(-40 -m_widhtAdd/2, -60, 80+m_widhtAdd, 104);

}

void interpretArgFuncIntroduced::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{

    QGraphicsObject::mouseMoveEvent(event);
    update();
    updateArrows();
}

void interpretArgFuncIntroduced::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mousePressEvent(event);
    this->setCursor(QCursor(Qt::ClosedHandCursor));
    Q_UNUSED(event);
}

void interpretArgFuncIntroduced::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{

    this->setCursor(QCursor(Qt::ArrowCursor));
    QGraphicsItem::mouseReleaseEvent(event);
    update();
}

void interpretArgFuncIntroduced::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    if (event->buttons() != Qt::LeftButton)
        return;

    QDialog *inputWindow = new QDialog(NULL);
    inputWindow->setGeometry(event->screenPos().x()-150, event->screenPos().y()-100, 300, 200);
    inputWindow->setModal(true);
    inputWindow->setWindowTitle("Введите значения оператора:");


    QSpinBox *m_constSpinBox = new QSpinBox;
    m_constSpinBox->setMaximum(2147483646);
    m_constSpinBox->setMinimum(-2147483647);
    m_constSpinBox->setValue(m_constanta);

    QLineEdit *m_funcNameLineEdit  = new QLineEdit;
    m_funcNameLineEdit->setText(m_funcName);
    QLabel *constLabel    = new QLabel("&Введите значение константы:");
    QLabel *funcLabel     = new QLabel("&Введите имя функции:");

    constLabel->setBuddy(m_constSpinBox);
    funcLabel->setBuddy(m_funcNameLineEdit);


    QPushButton* btn_Ok     = new QPushButton("&Ok");
    QPushButton* btn_Cancel = new QPushButton("&Cancel");

    connect(btn_Ok, SIGNAL(clicked(bool)), inputWindow, SLOT(accept()));
    connect(btn_Cancel, SIGNAL(clicked(bool)), inputWindow, SLOT(reject()));

    QGridLayout* layout = new QGridLayout;
        layout->addWidget(constLabel, 0, 0);
        layout->addWidget(funcLabel, 1, 0);
        layout->addWidget(m_constSpinBox, 0, 1);
        layout->addWidget(m_funcNameLineEdit, 1, 1);
        layout->addWidget(btn_Ok, 2,0);
        layout->addWidget(btn_Cancel, 2, 1);
        inputWindow->setLayout(layout);

    if(inputWindow->exec() == QDialog::Accepted)
    {
        if(!m_funcNameLineEdit->text().isEmpty())
            m_funcName = m_funcNameLineEdit->text();

        if(!m_constSpinBox->text().isEmpty())
            m_constanta = m_constSpinBox->text().toInt();
    }

    int count = m_funcName.size() - 12;
    if(count > 0)
    {
        m_widhtAdd = count*7;
    }
    else m_widhtAdd = 0;
    qDebug()<< "Razmer funcName :"<<m_funcName.size();

    oldLeftPoint = leftPoint;
    oldRightPoint = rightPoint;

    leftPoint.setX(-40 - m_widhtAdd/2);
    rightPoint.setX(40 + m_widhtAdd/2);
    setLinksPoints(leftPoint, 0);
    setLinksPoints(rightPoint, 2);

    if(getCountArrowOut()!=0)
    {
        arrowItem *arrow = getArrowOut(0);
        QPointF point = arrow->getStartPoint();
        if(point == oldLeftPoint)
            arrow->setStartPoint(leftPoint);

        if(point == oldRightPoint)
            arrow->setStartPoint(rightPoint);
    }

    delete m_constSpinBox;
    delete m_funcNameLineEdit;
    delete constLabel;
    delete funcLabel;
    delete btn_Ok;
    delete btn_Cancel;
    delete layout;
    delete inputWindow;
}

void interpretArgFuncIntroduced::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    scene()->clearSelection();
    setSelected(true);
    getContextMenu()->exec(event->screenPos());
}

void interpretArgFuncIntroduced::setArgFunc(int n, QString name)
{
    m_funcName = name;
    m_constanta = n;
}

QPixmap interpretArgFuncIntroduced::image()
{
    QPixmap pixmap(120,120);
    pixmap.fill(Qt::transparent);
    QPainter painter(&pixmap);
    painter.setRenderHint(QPainter::Antialiasing,true);
    painter.setPen(QPen(Qt::black,4));
    painter.setBrush(QColor(225,52,28));
    painter.translate(60, 70);
    QRect rectHigh(-40, -60, 80, 50);
    QRect rectDown(-40, -8, 80, 50);

    painter.drawRoundedRect(rectDown, 20, 20);
    painter.drawRect(rectHigh);
    return pixmap;
}

void interpretArgFuncIntroduced::setTypeOperator(int type)
{
    baseOperator::setTypeOperator(type);
}

int interpretArgFuncIntroduced::getTypeOperator()
{
    return baseOperator::getTypeOperator();
}

void interpretArgFuncIntroduced::saveToXML(QDomElement &cn)
{
    baseOperator::saveToXML(cn);
    cn.setAttribute("constanta", QString::number(m_constanta));
    cn.setAttribute("funcName", m_funcName);
    cn.setAttribute("widhtAdd", m_widhtAdd);

}

void interpretArgFuncIntroduced::loadFromXML(QDomElement &cn)
{
    baseOperator::loadFromXML(cn);
    m_constanta = cn.attribute("constanta").toInt();
    m_funcName = cn.attribute("funcName");
    m_widhtAdd = cn.attribute("widhtAdd").toInt();
}

int interpretArgFuncIntroduced::getValue()
{
    return m_constanta;
}

QString interpretArgFuncIntroduced::getFuncName()
{
    return m_funcName;
}

