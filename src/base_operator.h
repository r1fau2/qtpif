//#ifndef BASE_OPEATOR_H
//#define BASE_OPEATOR_H

#pragma once

#include "headers.h"
#include <QDomElement>

//
#include <QGraphicsWidget>
//

//#include "arrow_item.h"

class arrowItem;

enum TypeOperator
{
    type_entryFunction = 0,
    type_exitFunction,//1
    type_constant,//2
    type_copy_operator,//3
    type_external_function,//4
    type_identifier, //5
    type_interpreter,//6
    type_interpreter_arg,//7
    type_interpret_arg_func,//8
    type_interpret_func,//9
    type_groupList,//10
    type_groupParList,//11
    type_specSymbol,//12
    type_listArea,//13
    type_constantFunc,//14
    type_arrow//15
};

//class baseOperator : public QGraphicsObject
class baseOperator : public QGraphicsWidget
{
  //  Q_ENUMS(TypeOperator)

protected:
    int m_id;
    QList<arrowItem*> arrows;
    QList<arrowItem*> arrowsIn;
    QList<arrowItem*> arrowsOut;

    TypeOperator m_type;
    QRectF m_rect;//размер
    int width, height;//для изменения размеров
    int m_delayNumber;
    QMenu *m_contextMenu;
    int m_maxInLink; // -1 - нет ограничения, 0 - нет связей
    int m_maxOutLink; // -1 - нет ограничения, 0 - нет связей
    int m_currentInLink; // кол-во текущих связей
    int m_currentOutLink; // кол-во текущих связей
    QList<bool>    presenceLink;
    QList<QPointF> linksPoints;

//    virtual contextMenuEvent(QGraphicsSceneContextMenuEvent *event);
public:
    QColor m_color;
    baseOperator();
    int getMaxInLink();
    int getMaxOutLink();
    int getCurrentInLink();
    int getCurrentOutLink();
    void setMaxInLink(int const value);
    void setMaxOutLink(int const value);
    void setCurrentInLink(int const value);
    void setCurrentOutLink(int const value);
    void addPresenceLink(bool const value);
    void addLinksPoints(QPointF const point);
    QPointF getLinkPoint(int const n);
    bool getPresenceLink(int const n);
    void setPresenceLink(bool const value, int const n);
    void setLinksPoints(QPointF const point, int const n);
    int getCountLinksPoints();
    int getCountPresenceLink();

    arrowItem *getArrowIn(int const index);
    arrowItem *getArrowOut(int const index);

    int getCountArrowIn();
    int getCountArrowOut();

//    void hoverMoveEvent(QGraphicsSceneHoverEvent *event);
//    void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
    qreal getZ();
    void setDelayNumber(int const num);
    int getDelayNumber();
    void setId(int id);
    int getId();
    void setWidht(int const w);
    void setHeight(int const h);
    int getWidht();
    int getHeight();
    virtual QPixmap image() = 0;
    virtual void setTypeOperator(int type);
    virtual int getTypeOperator();
    int type();
    void addArrowIn(arrowItem *arrow);
    void addArrowOut(arrowItem *arrow);
    void removeArrowIn(arrowItem *arrow);
    void removeArrowOut(arrowItem *arrow);
    void addArrow(arrowItem *arrow);
    void removeArrow(arrowItem *arrow);
    void removeArrows();
    void updateArrows();
    void removeItem();
    void setContextMenu(QMenu *menu);
    QMenu *getContextMenu();
    virtual void saveToXML(QDomElement &cn);
    virtual void loadFromXML(QDomElement &cn);
    void saveArrows(QDomElement &cn);
};

//#endif // BASE_OPEATOR_H
