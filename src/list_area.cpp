#include "list_area.h"
#include <QInputDialog>
#include <QtWidgets>

listArea::listArea()
{
    setZValue(-5000);
    mousePressed = false;
    isResizing = false;
    m_top = m_downRight = QPointF(0,0);
    m_width = m_height = 30;
    m_type = type_listArea;
    m_color = QColor(240,240,240);
    setDelayNumber(1);
}

listArea::listArea(GraphScene *scene)
{
    setZValue(-5000);
    mousePressed = false;
    isResizing = false;
    m_top = m_downRight = QPointF(0,0);
    m_width = m_height = 30;
    m_type = type_listArea;
    m_scene = scene;
    m_color = QColor(240,240,240);
    setDelayNumber(1);
}

QRectF listArea::getBoundingRect()
{
    return boundingRect();
}

listArea::~listArea()
{

}

QRectF listArea::boundingRect() const
{
    return QRectF (QPointF(0.0, 0.0),QSize(m_width,m_height));
}

void listArea::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QColor fillColor = (option->state & QStyle::State_Selected) ? m_color.darker(150) : m_color;
    if (option->state & QStyle::State_MouseOver)
        fillColor = fillColor.lighter(125);

    painter->setRenderHint(QPainter::Antialiasing,true);

    painter->setPen(QPen(Qt::black, 2, Qt::DashLine));
    painter->setBrush(fillColor);
    painter->drawRect(0, 0, m_width, m_height);
    Q_UNUSED(widget);
}

void listArea::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsObject::mouseMoveEvent(event);
    update();
}

void listArea::mousePressEvent(QGraphicsSceneMouseEvent *event)
{

    //если не левая клавиша
    if (event->buttons() != Qt::LeftButton)
        return;

    this->setCursor(QCursor(Qt::ClosedHandCursor));
    QGraphicsItem::mousePressEvent(event);
    update();
}

void listArea::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{

    this->setCursor(QCursor(Qt::ArrowCursor));
    QGraphicsItem::mouseReleaseEvent(event);
    update();
    m_scene->areasUpdate();
    scene()->update();
    //m_scene->update();
}

void listArea::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{

    if (event->buttons() != Qt::LeftButton)
        return;
}

void listArea::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    scene()->clearSelection();
    setSelected(true);
    getContextMenu()->exec(event->screenPos());
}

QPixmap listArea::image()
{
    QPixmap pixmap(100,100);
    pixmap.fill(Qt::transparent);
    QPainter painter(&pixmap);
    painter.setRenderHint(QPainter::Antialiasing,true);
    painter.setPen(QPen(Qt::black, 4, Qt::DashLine));
    painter.setBrush(m_color.darker(150));
    painter.translate(50, 50);
    painter.drawRect(-30,-30,60,60);
    return pixmap;
}

void listArea::setTypeOperator(int type)
{
    baseOperator::setTypeOperator(type);
}

int listArea::getTypeOperator()
{
    return baseOperator::getTypeOperator();
}

void listArea::saveToXML(QDomElement &cn)
{

    baseOperator::saveToXML(cn);
    cn.setAttribute("top_x", QString::number(m_top.x()));
    cn.setAttribute("top_y", QString::number(m_top.y()));
    cn.setAttribute("widht", QString::number(m_width));
    cn.setAttribute("height", QString::number(m_height));


}

void listArea::loadFromXML(QDomElement &cn)
{
    baseOperator::loadFromXML(cn);
    m_top.setX(cn.attribute("top_x").toInt());
    m_top.setY(cn.attribute("top_y").toInt());
    m_width = cn.attribute("widht").toInt();
    m_height = cn.attribute("height").toInt();
}

