//#ifndef IDENTIFIER_H
//#define IDENTIFIER_H

#pragma once

//#include "headers.h"
#include "base_operator.h"

class identifier : public baseOperator
{
public:
    identifier();
    ~identifier();
protected:
    QString m_name;
    baseOperator *m_operator;
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);
    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);
public:
    virtual QPixmap image();
    virtual void setTypeOperator(int type);
    virtual int getTypeOperator();
    void saveToXML(QDomElement &cn);
    void loadFromXML(QDomElement &cn);
    void setOperator(baseOperator *obj);
    baseOperator *getOperator();
    void setName(QString const name);
    QString getName();

};

//#endif // IDENTIFIER_H
