﻿//#ifndef MAINWINDOW_H
//#define MAINWINDOW_H

#pragma once

#include <QMainWindow>
#include <QMenuBar>
#include <QMessageBox>
#include "graphscene.h"
#include "constant.h"
#include "entry_function.h"
#include "exit_from_function.h"
#include "external_function.h"
#include "identifier.h"
#include "operator_group_parallel_list.h"
#include "operator_group_list.h"
#include "interpreter.h"
#include "interpret_with_func_introduced.h"
#include "interpret_arg_func_introduced.h"
#include "copy_operator.h"
#include "spec_symbol.h"
#include "list_area.h"


QT_BEGIN_NAMESPACE
class QMenuBar;
class QAction;
class QToolBox;
class QSpinBox;
class QComboBox;
class QFontComboBox;
class QButtonGroup;
class QLineEdit;
class QGraphicsTextItem;
class QFont;
class QToolButton;
class QAbstractButton;
class QGraphicsView;
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void contentChanged(const bool& changed);
//     void setViewInterectiv(bool flag);
public slots:
     void releaseButtOperator(int);  //снять нажатие кнопки по её id

private slots:
     void about();
     void buttonGroupClicked(int id);
     void pointerGroupClicked(int selectMode);
     void deleteItems();
     void sceneScaleChanged(QString scale);
     void newProject();
     void openProject(const QString &fileName = "");
     void saveProject(const bool &checkIfReadonly = true);
     bool SaveProjectAtPath();
     void clearArea();
     void generateRIG();
     void closeWindow();
     bool closeProject();


private:

    void createActions();
    void createMenu();
    void createToolBox();
    void createToolbars();
    void setTitle(const QString &title);

    bool m_contentChanged;

    QWidget *addCellOperator(const QString &text, int id);
    QString m_fileName;
    GraphScene *m_scene;
    QGraphicsView *view;

    QMenuBar *menuBar;
    QMenu *fileMenu;
    QMenu *itemMenu;
    QMenu *aboutMenu;
    QMenu *actionMenu;
    //Меню File(fileMenu)
    QAction *exitAction;
    QAction *newFile;
    QAction *openFileAction;
    QAction *saveFileAction;
    QAction *saveFileToPathAction;
    QAction *closeFile;
    QAction *closeFileProgram;
    //Меню actionMenu
    QAction *clearAction;
    QAction *generate;
    //Меню о программе
    QAction *aboutAction;
    //
    QAction *deleteAction;

    QToolBar *editToolBar;
    QToolBar *pointerToolbar;

    QComboBox *sceneScaleCombo;

    QToolBox *toolBox;
    QButtonGroup *buttonGroup;
    QButtonGroup *pointerTypeGroup;
};

//#endif // MAINWINDOW_H
