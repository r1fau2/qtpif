//#ifndef LIST_AREA_H
//#define LIST_AREA_H

#pragma once

#include "base_operator.h"
#include "graphscene.h"

class listArea : public baseOperator
{
public:
    listArea();

    listArea(GraphScene *scene);
    QRectF getBoundingRect();
    bool mousePressed;
    bool isResizing;
    int m_width, m_height;
    QPointF m_top;
    QPointF m_downRight;
    bool calc;
    ~listArea();
protected:

    GraphScene *m_scene;
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);
    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);
public:
    virtual QPixmap image();
    virtual void setTypeOperator(int type);
    virtual int getTypeOperator();
    void saveToXML(QDomElement &cn);
    void loadFromXML(QDomElement &cn);

};

//#endif // LIST_AREA_H
