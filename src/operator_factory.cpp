#include <QFileDialog>
#include <QtXml>
#include <QMessageBox>
#include <QMenu>

#include "operator_factory.h"
#include "constant.h"
#include "entry_function.h"
#include "exit_from_function.h"
#include "copy_operator.h"
#include "external_function.h"
#include "identifier.h"
#include "interpret_with_func_introduced.h"
#include "interpret_arg_func_introduced.h"
#include "interpreter.h"
#include "operator_group_list.h"
#include "operator_group_parallel_list.h"
#include "graphscene.h"
#include "interpret_with_arg.h"
#include "spec_symbol.h"
#include "list_area.h"
#include "container.h"
#include "arrow_item.h"
#include "constantFunc.h"

baseOperator *operator_factory::createItem(GraphScene *scene, int type)
{
    baseOperator *newItem = NULL;
    switch (type) {
    case 0:
        newItem = new entryFunction();
        qDebug()<<"Created item with id=0"<<endl;
        break;
    case 1:
        newItem = new exitFromFunction();
        qDebug()<<"Created item with id=1"<<endl;
        break;
    case 2:
        newItem = new Constant(scene);
        qDebug()<<"Created item with id=2"<<endl;
        break;
    case 3:
        newItem = new copyOperator();
        qDebug()<<"Created item with id=3"<<endl;
        break;
    case 4:
        newItem = new external_function();
        qDebug()<<"Created item with id=4"<<endl;
        break;
    case 5:
        newItem = new identifier();
        qDebug()<<"Created item with id=5"<<endl;
        break;
    case 6:
        newItem = new interpreter();
        qDebug()<<"Created item with id=6"<<endl;
        break;
    case 7:
        newItem = new interpretWithArg();
        qDebug()<<"Created item with id=7"<<endl;
        break;
    case 8:
        newItem = new interpretArgFuncIntroduced();
        qDebug()<<"Created item with id=8"<<endl;
        break;
    case 9:
        newItem = new interpretWithFuncIntroduced();
        qDebug()<<"Created item with id=9"<<endl;
        break;
    case 10:
        newItem = new operatorGroupList();
        qDebug()<<"Created item with id=10"<<endl;
        break;
    case 11:
        newItem = new operatorGroupParallelList();
        qDebug()<<"Created item with id=11"<<endl;
        break;
    case 12:
        newItem = new specSymbol();
        qDebug()<<"Created item with id=12"<<endl;
        break;
    case 13:
        newItem = new listArea();
        qDebug()<<"Created item with id=13"<<endl;
        break;
    case 14:
        newItem = new constantFunc();
        qDebug()<<"Created item with id=14"<<endl;
        break;
    default:
        break;
    }

    return newItem;
}

bool operator_factory::createFromXML(const QString &fileName, GraphScene *scene, QMenu *contextMenu)
{
    // open & parse XML file
    QDomDocument doc("PifagorIDE");
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly))
    {
        qDebug()<<"не удалось прочесть файл";
        return false;
    }

    if (!doc.setContent(&file))
    {
         qDebug()<<"Ошибка при парсинге XML файла";
        file.close();
        return false;
    }
    file.close();

    container *cont = container::instance();
    QDomElement docElem = doc.documentElement();

    // add nodes
    QDomNodeList operators = docElem.childNodes().item(0).childNodes();
    for (int i = 0; i < operators.length(); i++)
    {
        QDomElement e = operators.item(i).toElement();
        if(!e.isNull())
        {
            baseOperator *newItem = NULL;
            int type = e.attribute("TypeOperator").toInt();
            newItem = createItem(scene,type);
            newItem->loadFromXML(e);
            newItem->setContextMenu(contextMenu);
            scene->addItem(newItem);
            newItem->update();
            cont->listItem.append(newItem);
        }
    }

    QDomNodeList areas = docElem.childNodes().item(1).childNodes();
    for (int i = 0; i < areas.length(); i++)
    {
        QDomElement e = areas.item(i).toElement();
        if(!e.isNull())
        {
            baseOperator *newItem = NULL;
            int type = e.attribute("TypeOperator").toInt();
            newItem = createItem(scene,type);
            newItem->loadFromXML(e);
            newItem->setContextMenu(contextMenu);
            scene->addItem(newItem);
            newItem->update();
            cont->listItem.append(newItem);
            cont->listAreas.append(qgraphicsitem_cast<listArea*>(newItem));
        }
    }

    QDomNodeList arrows = docElem.childNodes().item(2).childNodes();
    for (int i = 0; i < arrows.length(); i++)
    {
        QDomElement e = arrows.item(i).toElement();
        if(!e.isNull())
        {
            arrowItem *item = new arrowItem();
            item->loadFromXML(e);
            scene->addItem(item);
            cont->listArrows.append(item);
        }
    }


    return true;
}

void operator_factory::saveToXML(const QString &fileName)
{
    container *cont = container::instance();
    for(int i = 0; i < cont->listItem.size(); i++)
    {
        if(cont->listItem.at(i)->getTypeOperator() == type_copy_operator)
        {
            cont->listItem.append(cont->listItem.at(i));
            cont->listItem.removeAt(i);
        }
    }
    QDomDocument doc("PifagorIDE");

    QDomElement root = doc.createElement("PIFAGOR");
    doc.appendChild( root );

    // nodes
    int index = 0;
    QDomElement operators = doc.createElement("operators");
    root.appendChild(operators);
    foreach(baseOperator *obj, cont->listItem)
    {
        if(obj->getTypeOperator() != type_listArea)
        {
            QDomElement cn = doc.createElement("operator");
            obj->setId(index);
            index++;
            obj->saveToXML(cn);
            operators.appendChild(cn);
        }
    }

    QDomElement areas = doc.createElement("areas");
    root.appendChild(areas);
    foreach(listArea *obj, cont->listAreas)
    {
        QDomElement cn = doc.createElement("area");
        obj->setId(index);
        index++;
        obj->saveToXML(cn);
        areas.appendChild(cn);
    }

    QDomElement arrows = doc.createElement("arrows");
    root.appendChild(arrows);

    foreach(arrowItem *obj, cont->listArrows)
    {
        //obj->saveArrows(cn);
        QDomElement cn = doc.createElement("arrow");
        obj->saveToXML(cn);
        arrows.appendChild(cn);
    }

    // write XML doc object to file
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly))
    {
        qDebug()<<"Не записалось!";
        return;
    }
    QTextStream ts( &file );
    ts << doc.toString();
    file.close();
}
