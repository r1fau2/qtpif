#include "copy_operator.h"
#include <QtWidgets>
#include "container.h"

copyOperator::copyOperator()
{
    setFlags(ItemIsSelectable | ItemIsMovable);
    setAcceptHoverEvents(true);
    m_type = type_copy_operator;
    m_color = QColor(Qt::black);
    addLinksPoints(QPoint(0,-10));
    addLinksPoints(QPoint(0,0));
    addPresenceLink(false);
    addPresenceLink(false);
    setMaxInLink(1);
    setMaxOutLink(-1);
    m_operator = NULL;
}


copyOperator::~copyOperator()
{

}

void copyOperator::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QColor fillColor = (option->state & QStyle::State_Selected) ? m_color.darker(200) : m_color;
    if (option->state & QStyle::State_MouseOver)
        fillColor = fillColor.lighter(125);

     painter->setRenderHint(QPainter::Antialiasing,true);
     painter->setPen(QPen(Qt::black,1.5));
     painter->setBrush(fillColor);

     painter->drawEllipse(-10,-10,20,20);

     Q_UNUSED(option);
     Q_UNUSED(widget);
}

QRectF copyOperator::boundingRect() const
{
    return QRectF(-10,-10,20+2,20+2);
    //2 - толщина пера
}

void copyOperator::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{

    QGraphicsObject::mouseMoveEvent(event);
    update();
    updateArrows();
}

void copyOperator::mousePressEvent(QGraphicsSceneMouseEvent *event)
{

    QGraphicsItem::mousePressEvent(event);
    this->setCursor(QCursor(Qt::ClosedHandCursor));
    Q_UNUSED(event);
}

void copyOperator::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{

    this->setCursor(QCursor(Qt::ArrowCursor));
    QGraphicsItem::mouseReleaseEvent(event);
    update();
}

void copyOperator::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    scene()->clearSelection();
    setSelected(true);
    getContextMenu()->exec(event->screenPos());
}

QPixmap copyOperator::image()
{
    QPixmap pixmap(70,70);
    pixmap.fill(Qt::transparent);
    QPainter painter(&pixmap);
    painter.setRenderHint(QPainter::Antialiasing,true);
    painter.setPen(QPen(Qt::black,1.5));
    painter.setBrush(Qt::black);
    painter.translate(35, 35);
    painter.drawEllipse(-10,-10,20,20);
    return pixmap;
}

void copyOperator::setTypeOperator(int type)
{
    baseOperator::setTypeOperator(type);
}

int copyOperator::getTypeOperator()
{
    return baseOperator::getTypeOperator();
}

void copyOperator::saveToXML(QDomElement &cn)
{
    cn.setAttribute("operatorId", QString::number(m_operator->getId()));
    baseOperator::saveToXML(cn);
}

void copyOperator::loadFromXML(QDomElement &cn)
{
    int id = cn.attribute("operatorId").toInt();
    container *cont = container::instance();
    for(int i = 0; i < cont->listItem.size(); i++)
    {
        if(cont->listItem.at(i)->getId() == id)
            m_operator = cont->listItem.at(i);
    }
    baseOperator::loadFromXML(cn);
}

baseOperator *copyOperator::getOperator()
{
    if(m_operator == NULL)
        return 0;
    else
        return m_operator;
}

void copyOperator::setOperator(baseOperator *op)
{
    m_operator = op;
}

bool copyOperator::checkOperator()
{
    if(m_operator == NULL)
        return false;
    else
        return true;
}

