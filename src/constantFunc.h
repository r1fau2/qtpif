//#ifndef CONSTANT_FUNC_H
//#define CONSTANT_FUNC_H

#pragma once

#include "base_operator.h"

class constantFunc : public baseOperator
{
public:
    constantFunc();
    ~constantFunc();
protected:
    QString m_smb;
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);
    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);
public:
    virtual QPixmap image();
    virtual void setTypeOperator(int type);
    virtual int getTypeOperator();
    void saveToXML(QDomElement &cn);
    void loadFromXML(QDomElement &cn);
    QString getValue();

};

//#endif // CONSTANT_FUNC_H
