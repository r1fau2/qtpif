//#ifndef INTERPRET_ARG_FUNC_INTRODUCED_H
//#define INTERPRET_ARG_FUNC_INTRODUCED_H

#pragma once

//#include "headers.h"
#include "base_operator.h"

class interpretArgFuncIntroduced : public baseOperator
{
public:
    interpretArgFuncIntroduced();
    ~interpretArgFuncIntroduced();
protected:
    int m_widhtAdd;
    int m_constanta;
    QString m_funcName;
    QPointF leftPoint;
    QPointF rightPoint;
    QPointF oldLeftPoint;
    QPointF oldRightPoint;
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);
    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);
public slots:
    void setArgFunc(int n, QString name);
public:
    virtual QPixmap image();
    virtual void setTypeOperator(int type);
    virtual int getTypeOperator();
    void saveToXML(QDomElement &cn);
    void loadFromXML(QDomElement &cn);
    int getValue();
    QString getFuncName();
};

//#endif // INTERPRET_ARG_FUNC_INTRODUCED_H
