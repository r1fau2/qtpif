//#ifndef COPY_OPERATOR_H
//#define COPY_OPERATOR_H

#pragma once

#include "base_operator.h"


class copyOperator : public baseOperator
{
public:
    copyOperator();
    ~copyOperator();
protected:
    baseOperator *m_operator;
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);

public:
    virtual QPixmap image();
    virtual void setTypeOperator(int type);
    virtual int getTypeOperator();
    void saveToXML(QDomElement &cn);
    void loadFromXML(QDomElement &cn);
    baseOperator *getOperator();
    void setOperator(baseOperator* op);
    bool checkOperator();
};

//#endif // COPY_OPERATOR_H
