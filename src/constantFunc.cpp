#include "constantFunc.h"
#include "container.h"
#include <QInputDialog>
#include <QRegExpValidator>
#include <QLineEdit>
#include <QLabel>
#include <QPushButton>
#include <QGridLayout>
#include <QtWidgets>


constantFunc::constantFunc()
{
    m_type = type_constantFunc;
    m_color = QColor(Qt::green);
    addLinksPoints(QPoint(-30,0));
    addLinksPoints(QPoint(0,-30));
    addLinksPoints(QPoint(30,0));
    addLinksPoints(QPoint(0,30));
    addPresenceLink(false);
    addPresenceLink(false);
    addPresenceLink(false);
    addPresenceLink(false);
    setMaxInLink(0);
    setMaxOutLink(1);
}

constantFunc::~constantFunc()
{

}

void constantFunc::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QColor fillColor = (option->state & QStyle::State_Selected) ? m_color.darker(150) : m_color;
    if (option->state & QStyle::State_MouseOver)
        fillColor = fillColor.lighter(125);

    painter->setRenderHint(QPainter::Antialiasing,true);
    painter->setPen(QPen(Qt::black,1.5));
    painter->setBrush(fillColor);
    painter->drawEllipse(-30,-30,60,60);
    QFont font = painter->font();
    font.setPixelSize(20);
    painter->setFont(font);
    painter->drawText(this->boundingRect(),Qt::AlignCenter,m_smb);
    Q_UNUSED(option);
    Q_UNUSED(widget);
}

QRectF constantFunc::boundingRect() const
{
    return QRectF (-30,-30,60,60);
}

void constantFunc::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsObject::mouseMoveEvent(event);
    update();
    updateArrows();
}

void constantFunc::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mousePressEvent(event);
    this->setCursor(QCursor(Qt::ClosedHandCursor));
    if (event->buttons() != Qt::LeftButton)
    {
        if(event->buttons() == Qt::RightButton)
        {
            container *c = container::instance();
            c->listItem.removeAll(this);
            this->~constantFunc();
            return;
        }
    }
    else return;
    Q_UNUSED(event);
}

void constantFunc::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    this->setCursor(QCursor(Qt::ArrowCursor));
    QGraphicsItem::mouseReleaseEvent(event);

    update();
}

void constantFunc::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
 /*   QDialog *inputWindow = new QDialog(NULL);
    inputWindow->setGeometry(event->screenPos().x()-150, event->screenPos().y()-100, 300, 200);
    inputWindow->setModal(true);

    //QRegExp rx("(.|+|-|*|/|%|..|dup|<|<=|=|!=|>=|>|?|#|true|false|type|value|bool|char|int|float|string|()|[]|{}|<()|error|in)");
    QRegExp rx("[.+-*|/|%|..|dup|<|<=|=|!=|>=|>|?|#|true|false|type|value|bool|char|int|float|string|()|[]|{}|<()|error|in]");
    QValidator *validator = new QRegExpValidator(rx, this);


    QLineEdit *m_lineEdit  = new QLineEdit(inputWindow);
    m_lineEdit->setText(m_smb);
    m_lineEdit->setValidator(validator);
    QLabel *m_lineLabel     = new QLabel("&Введите имя функции:");
    m_lineLabel->setBuddy(m_lineEdit);

    QPushButton* btn_Ok     = new QPushButton("&Ok");
    QPushButton* btn_Cancel = new QPushButton("&Cancel");

    connect(btn_Ok, SIGNAL(clicked(bool)), inputWindow, SLOT(accept()));
    connect(btn_Cancel, SIGNAL(clicked(bool)), inputWindow, SLOT(reject()));

    QGridLayout* layout = new QGridLayout;
        layout->addWidget(m_lineLabel, 0, 0);
        layout->addWidget(m_lineEdit, 0, 1);
        layout->addWidget(btn_Ok, 1,0);
        layout->addWidget(btn_Cancel, 1, 1);
        inputWindow->setLayout(layout);

    if(inputWindow->exec() == QDialog::Accepted)
    {
        if(!m_lineEdit->text().isEmpty())
            m_smb = m_lineEdit->text();
    }

    delete m_lineEdit;
    delete m_lineLabel;
    delete btn_Ok;
    delete btn_Cancel;
    delete layout;
    delete inputWindow;
    */

    if (event->buttons() != Qt::LeftButton)
        return;

    QString str[] = {"+", "-", "*", "/", "%", "<", "<=", "=", "!=", ">=", ">", "true", "false", "-1"};
    QStringList items;

    for(int i=0; str[i]!="-1"; i++)
          items << str[i];

          bool ok;
          QString buf = QInputDialog::getItem(NULL, tr("Константа"),
                                               tr("Выберите:"), items, 0, false, &ok);
          if (ok && !buf.isEmpty())
              m_smb = buf;
}

void constantFunc::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    scene()->clearSelection();
    setSelected(true);
    getContextMenu()->exec(event->screenPos());
}

QPixmap constantFunc::image()
{
    QPixmap pixmap(100,100);
    pixmap.fill(Qt::transparent);
    QPainter painter(&pixmap);
    painter.setRenderHint(QPainter::Antialiasing,true);
    painter.setPen(QPen(Qt::black,4));
    painter.setBrush(QColor(Qt::green));
    painter.translate(50, 50);
    painter.drawEllipse(-30,-30,60,60);

    return pixmap;
}

void constantFunc::setTypeOperator(int type)
{
    baseOperator::setTypeOperator(type);
}

int constantFunc::getTypeOperator()
{
    return baseOperator::getTypeOperator();
}

void constantFunc::saveToXML(QDomElement &cn)
{
    baseOperator::saveToXML(cn);
    cn.setAttribute("data", m_smb);
}

void constantFunc::loadFromXML(QDomElement &cn)
{
    baseOperator::loadFromXML(cn);
    m_smb = cn.attribute("data");
}

QString constantFunc::getValue()
{
    return m_smb;
}

