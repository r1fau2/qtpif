#include "constant.h"
#include <QInputDialog>
#include <QtWidgets>

Constant::Constant()
{
    m_value = 0;
    m_color = QColor(Qt::green).darker(150);
    m_type = type_constant;
    addLinksPoints(QPoint(-30,0));
    addLinksPoints(QPoint(30,0));
    addLinksPoints(QPoint(0,30));
    addPresenceLink(false);
    addPresenceLink(false);
    addPresenceLink(false);
    setMaxInLink(0);
    setMaxOutLink(1);
}

Constant::Constant(QGraphicsScene *scene)
{
    m_value = 0;
    m_scene = scene;
    m_color = QColor(Qt::green);
    m_type = type_constant;
    addLinksPoints(QPoint(-30,0));
    addLinksPoints(QPoint(30,0));
    addLinksPoints(QPoint(0,30));
    addPresenceLink(false);
    addPresenceLink(false);
    addPresenceLink(false);
    setMaxInLink(0);
    setMaxOutLink(1);
    setContextMenu(NULL);
}

Constant::~Constant()
{

}

QRectF Constant::boundingRect() const
{
    return QRectF (-30,-30,60,60);
}

void Constant::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{

    QColor color = m_color;
    QColor fillColor = (option->state & QStyle::State_Selected) ? color.darker(150) : color;
    if (option->state & QStyle::State_MouseOver)
        fillColor = fillColor.lighter(125);
 //   QPointF inputPoints[3] = { QPointF(-30.0, 0.0), QPointF(0.0, 30.0), QPointF(30.0), 0.0 };
    painter->setRenderHint(QPainter::Antialiasing,true);
    painter->setPen(Qt::black);
    painter->setBrush(fillColor);
    painter->drawRect(-30,-30,60,60);
    painter->drawText(this->boundingRect(),Qt::AlignCenter,QString::number(m_value));
   // qDebug()<<"Type: "<<this->type();
    //qDebug()<<"cont paint. Pos: "<<this->pos()<<endl;
    Q_UNUSED(widget);


}

void Constant::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{

    QGraphicsObject::mouseMoveEvent(event);

    updateArrows();

}

void Constant::mousePressEvent(QGraphicsSceneMouseEvent *event)
{

    //если не левая клавиша
    if (event->buttons() != Qt::LeftButton)
        return;

/*    if(event->modifiers() == Qt::ShiftModifier) {
        // выделить
        setSelected(true);
    } */
    QGraphicsItem::mousePressEvent(event);
  //  qDebug()<<"Координаты: "<<event->pos();
    this->setCursor(QCursor(Qt::ClosedHandCursor));
    update();

}

void Constant::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    this->setCursor(QCursor(Qt::ArrowCursor));

    QGraphicsItem::mouseReleaseEvent(event);

    update();

}

void Constant::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{

    if (event->buttons() != Qt::LeftButton)
        return;
    bool ok; QString str;
    int value = QInputDialog::getInt(NULL, tr("Константа:"),"Значение:"  , m_value, -2147483647, 2147483647, 1, &ok, Qt::Sheet);

    if(ok)
        m_value = value;
}

void Constant::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    m_scene->clearSelection();
    setSelected(true);
    getContextMenu()->exec(event->screenPos());
}


QPixmap Constant::image()
{
    QPixmap pixmap(100,100);
    pixmap.fill(Qt::transparent);
    QPainter painter(&pixmap);
    painter.setRenderHint(QPainter::Antialiasing,true);
    painter.setPen(QPen(Qt::black,4));
    painter.setBrush(Qt::green);
    painter.translate(50, 50);
    painter.drawRect(-30,-30,60,60);
    return pixmap;
}

void Constant::setTypeOperator(int type)
{
    baseOperator::setTypeOperator(type);
}

int Constant::getTypeOperator()
{
    return baseOperator::getTypeOperator();
}

void Constant::saveToXML(QDomElement &cn)
{
    baseOperator::saveToXML(cn);
    cn.setAttribute("value", QString::number(m_value));
}

void Constant::loadFromXML(QDomElement &cn)
{
    baseOperator::loadFromXML(cn);
    m_value = cn.attribute("value").toInt();
}

int Constant::getValue()
{
    return m_value;
}
