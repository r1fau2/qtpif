#include "interpreter.h"
#include <QtWidgets>


interpreter::interpreter()
{
    m_type = type_interpreter;
    m_color = QColor(225,52,28);
    addLinksPoints(QPoint(-30,0));
    addLinksPoints(QPoint(0,-20));
    addLinksPoints(QPoint(30,0));
    addLinksPoints(QPoint(0,20));
    addPresenceLink(false);
    addPresenceLink(false);
    addPresenceLink(false);
    addPresenceLink(false);
    setMaxInLink(2);
    setMaxOutLink(1);
}

interpreter::~interpreter()
{

}

void interpreter::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{

    QColor fillColor = (option->state & QStyle::State_Selected) ? m_color.darker(150) : m_color;
    if (option->state & QStyle::State_MouseOver)
        fillColor = fillColor.lighter(125);


    painter->setRenderHint(QPainter::Antialiasing,true);
    painter->setPen(QPen(Qt::black,2));
    painter->setBrush(fillColor);

    const QPointF points[4] = {
                    QPointF(0, -20),
                    QPointF(-30, 0),
                    QPointF(0, 20),
                    QPointF(30, 0)
    };

    painter->drawConvexPolygon(points, 4);

    Q_UNUSED(option);
    Q_UNUSED(widget);
}

QRectF interpreter::boundingRect() const
{
    return QRectF(-30,-20,60+2,40+2);
    //2 - толщина пера
}

void interpreter::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{

    QGraphicsObject::mouseMoveEvent(event);
    update();
    updateArrows();
}

void interpreter::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mousePressEvent(event);
    qDebug()<<"Координаты: "<<event->pos();
    this->setCursor(QCursor(Qt::ClosedHandCursor));
    Q_UNUSED(event);
}

void interpreter::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{

    this->setCursor(QCursor(Qt::ArrowCursor));
    QGraphicsItem::mouseReleaseEvent(event);
    update();
}

void interpreter::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    scene()->clearSelection();
    setSelected(true);
    getContextMenu()->exec(event->screenPos());
}

QPixmap interpreter::image()
{
    QPixmap pixmap(100,100);
    pixmap.fill(Qt::transparent);
    QPainter painter(&pixmap);
    painter.setRenderHint(QPainter::Antialiasing,true);
    painter.setPen(QPen(Qt::black,4));
    painter.setBrush(QColor(225,52,28));
    painter.translate(50, 50);
    const QPointF points[4] = {
                    QPointF(0, -20),
                    QPointF(-30, 0),
                    QPointF(0, 20),
                    QPointF(30, 0)
    };

    painter.drawConvexPolygon(points, 4);
    return pixmap;
}

void interpreter::setTypeOperator(int type)
{
    baseOperator::setTypeOperator(type);
}

int interpreter::getTypeOperator()
{
    return baseOperator::getTypeOperator();
}

void interpreter::saveToXML(QDomElement &cn)
{
    baseOperator::saveToXML(cn);
}

void interpreter::loadFromXML(QDomElement &cn)
{
    baseOperator::loadFromXML(cn);
}

