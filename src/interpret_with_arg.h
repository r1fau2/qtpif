//#ifndef INTERPRET_WITH_ARG_H
//#define INTERPRET_WITH_ARG_H

#pragma once

#include "base_operator.h"

class interpretWithArg : public baseOperator
{
public:
    interpretWithArg();
    ~interpretWithArg();
protected:
    int m_value;
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);
    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);
public:
    virtual QPixmap image();
    virtual void setTypeOperator(int type);
    virtual int getTypeOperator();
    void saveToXML(QDomElement &cn);
    void loadFromXML(QDomElement &cn);
    int getValue();
};

//#endif // INTERPRET_WITH_ARG_H
