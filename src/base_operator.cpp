#include "base_operator.h"
#include "arrow_item.h"
#include "container.h"
#include <QString>
baseOperator::baseOperator()
{
    setAcceptHoverEvents(true);
    setFlags( QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable);
    setFiltersChildEvents(true);
    m_delayNumber = 0;
    height = 0;
    width = 0;
    m_maxInLink = 0;
    m_maxOutLink = 0;
    m_currentInLink = 0;
    m_currentOutLink = 0;
}

int baseOperator::getMaxInLink()
{
    return m_maxInLink;
}

int baseOperator::getMaxOutLink()
{
    return m_maxOutLink;
}

int baseOperator::getCurrentInLink()
{
    return m_currentInLink;
}

int baseOperator::getCurrentOutLink()
{
    return m_currentOutLink;
}

void baseOperator::setMaxInLink(const int value)
{
    m_maxInLink = value;
}

void baseOperator::setMaxOutLink(const int value)
{
    m_maxOutLink = value;
}

void baseOperator::setCurrentInLink(const int value)
{
    m_currentInLink = value;
}

void baseOperator::setCurrentOutLink(const int value)
{
    m_currentOutLink = value;
}

void baseOperator::addPresenceLink(const bool value)
{
    presenceLink.append(value);
}

void baseOperator::addLinksPoints(const QPointF point)
{
    linksPoints.append(point);
}

QPointF baseOperator::getLinkPoint(const int n)
{
    return linksPoints.at(n);
}

bool baseOperator::getPresenceLink(const int n)
{
    return presenceLink.at(n);
}

void baseOperator::setPresenceLink(bool const value, const int n)
{
    presenceLink.replace(n, value);
}

void baseOperator::setLinksPoints(QPointF const point, const int n)
{
    linksPoints.replace(n, point);
}

int baseOperator::getCountLinksPoints()
{
    return linksPoints.size();
}

int baseOperator::getCountPresenceLink()
{
    return presenceLink.size();
}

arrowItem *baseOperator::getArrowIn(const int index)
{
    return arrowsIn.at(index);
}

arrowItem *baseOperator::getArrowOut(const int index)
{
    return arrowsOut.at(index);
}

int baseOperator::getCountArrowIn()
{
    return arrowsIn.size();
}

int baseOperator::getCountArrowOut()
{
    return arrowsOut.size();
}

//void baseOperator::hoverMoveEvent(QGraphicsSceneHoverEvent *event)
//{
//    qDebug()<<event->pos();
//}

//void baseOperator::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
//{
//    qDebug()<<"Внутри";
//}

qreal baseOperator::getZ()
{
    return QGraphicsItem::zValue();
}

void baseOperator::setDelayNumber(const int num)
{
    m_delayNumber = num;
}

int baseOperator::getDelayNumber()
{
    return m_delayNumber;
}

void baseOperator::setId(int id)
{
    m_id = id;
}

int baseOperator::getId()
{
    return m_id;
}

void baseOperator::setWidht(const int w)
{
    width = w;
}

void baseOperator::setHeight(const int h)
{
    height = h;
}

int baseOperator::getWidht()
{
    return width;
}

int baseOperator::getHeight()
{
    return height;
}

void baseOperator::setTypeOperator(int type)
{
    m_type = TypeOperator(type);
}

int baseOperator::getTypeOperator()
{
    return (int)m_type;
}

int baseOperator::type()
{
    return m_type;
}

void baseOperator::addArrowIn(arrowItem *arrow)
{
    arrowsIn.append(arrow);
    m_currentInLink++;
}

void baseOperator::addArrowOut(arrowItem *arrow)
{
    arrowsOut.append(arrow);
    m_currentOutLink++;
}

void baseOperator::removeArrowIn(arrowItem *arrow)
{
    int index = arrowsIn.indexOf(arrow);
    if (index != -1)
    {
        arrowsIn.removeAt(index);
        m_currentInLink--;
    }
}

void baseOperator::removeArrowOut(arrowItem *arrow)
{
    int index = arrowsOut.indexOf(arrow);
    if (index != -1)
    {
        arrowsOut.removeAt(index);
        m_currentOutLink--;
    }
}

void baseOperator::addArrow(arrowItem *arrow)
{
    arrows.append(arrow);
}

void baseOperator::removeArrow(arrowItem *arrow)
{
    int index = arrows.indexOf(arrow);
    if (index != -1)
        arrows.removeAt(index);
}

void baseOperator::removeArrows()
{

    foreach (arrowItem *arrow, arrowsIn)
    {
            arrow->removeArrow(arrow);
            delete arrow;
    }

    foreach (arrowItem *arrow, arrowsOut)
    {
            arrow->removeArrow(arrow);
            delete arrow;
    }
}

void baseOperator::updateArrows()
{
//    foreach (arrowItem *arrow, arrows)
//    {
//        arrow->updatePosition();
//    }

    foreach (arrowItem *arrow, arrowsOut)
    {
        arrow->updatePosition();
    }

    foreach (arrowItem *arrow, arrowsIn)
    {
        arrow->updatePosition();
    }
}

void baseOperator::removeItem()
{
    //удаление элемента и если это зона выделения, то удаление её из своего контейнера
    container *cont = container::instance();
    int index =  cont->listItem.indexOf(this);
    if(index != -1)
    {
        this->removeArrows();
        cont->listItem.removeAt(index);
    }
}

void baseOperator::setContextMenu(QMenu *menu)
{
    m_contextMenu = menu;
}

QMenu *baseOperator::getContextMenu()
{
    return m_contextMenu;
}

void baseOperator::saveToXML(QDomElement &cn)
{
    cn.setAttribute("id", QString::number(m_id));
    cn.setAttribute("TypeOperator", QString::number(m_type));
    cn.setAttribute("x", QString::number(this->pos().x()));
    cn.setAttribute("y", QString::number(this->pos().y()));
    cn.setAttribute("delayNumber", QString::number(m_delayNumber));
  //  cn.setAttribute("currentInLinks", QString::number(m_currentInLink));
  //  cn.setAttribute("currentOutLinks", QString::number(m_currentOutLink));

}

void baseOperator::loadFromXML(QDomElement &cn)
{
    setPos(cn.attribute("x").toInt(),cn.attribute("y").toInt());
    m_id = cn.attribute("id").toInt();
    m_delayNumber = cn.attribute("delayNumber").toInt();
 //   m_currentInLink = cn.attribute("currentInLinks").toInt();
 //   m_currentOutLink = cn.attribute("currentOutLinks").toInt();
}

void baseOperator::saveArrows(QDomElement &cn)
{
    if(arrows.size() == 0)
        return;

    foreach (arrowItem *item, arrows)
    {
            item->saveToXML(cn);
    }
}


